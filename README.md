# Stax
## A player cooler than ever, based on Blender

### Presentation
Stax is a player which allows you to review shots, assets (soon)... You can approve, send it to retake with written 
notes or on-picture drawn comments.
It's based on the [Application Template](https://docs.blender.org/manual/en/dev/advanced/app_templates.html) Blender system.

Find more information on the public page: https://superprod.gitlab.io/stax_suite/stax/

Read the documentation: https://superprod.gitlab.io/stax_suite/stax/docs/

Now developed by Félix David.  
First working version was made by Jean-François Sarazin.
