Comment
-------

You can create a new comment using ``Write Comment`` button. The sequencer will zoom to the media and a preview range is set to the media boundaries, loop playing the current media only.

.. figure:: ../images/Comment_Write_icon.png
            :alt: Write Comment
            :scale: 50 %
            
            Write Comment

Type your comment and click on ``Confirm`` or abort with ``Cancel``.

See `how to hide/show comments`_.

.. _`how to hide/show comments`: :ref:`view the comments`

.. list-table:: 

   * - .. figure:: ../images/Comment_New-Starting.jpg
            :alt: New comment
            :width: 600

            New Comment

   * - .. figure:: ../images/Comment_New-Writing.jpg
            :alt: Write Comment
            :width: 600

            Write Comment

   * - .. figure:: ../images/Comment_New-Pending.jpg
            :alt: Pending Comment
            :width: 600

            Pending Comment

Related to a range
^^^^^^^^^^^^^^^^^^
If your comment targets a specific part of the media, you can decide to change the handles to the wanted range. This will add the comment as a text annotation.

.. figure:: ../images/Comment_Annotation-Write.jpg
   :alt: New Text Annotation
   :width: 600

   New Text Annotation

If your comment is related to the current frame only, it's easier to click on the ``Current Frame Only`` button, the comment range will be set to the current frame.

.. figure:: ../images/Comment_CurrentFrameOnly.png
   :alt: Current Frame Only Button

   Current Frame Only Button


Pending note
^^^^^^^^^^^^^^^^
In the ``Pending Note`` panel you can see the pending text comments and annotations, choose to edit or delete them.

.. figure:: ../images/Comment_Annotation-Edit.png
   :alt: Edit Text Annotation

   Edit Text Annotation


Reply to a comment
^^^^^^^^^^^^^^^^^^
When the comment panel is open, you can reply to a comment by clicking on ``Reply``. 
This will open a window to write your answer.

.. important::
   A reply works like a normal comment, it's not sent until you :ref:`publish`.

.. figure:: ../images/Comment_Reply.png
   :alt: Reply

   Reply