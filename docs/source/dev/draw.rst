=====================
Draw (OpenGL overlay)
=====================

Sequencer
==========================
``draw_sequencer.py``

.. automodule:: stax.draw.draw_sequencer
   :members:
   :undoc-members:
   :show-inheritance: