# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""Run pytest tests."""

import os
from pathlib import Path
import platform
import subprocess
import tempfile

import blender_addon_tester as BAT


def setup_stax_test_template(repo_root: Path) -> Path:
    """
    When launching tests, copy this repository and use it as the Stax template
    to ensure we are testing current changes.

    :param repo_root: Path of the root of the repository
    :return: Path of the temp copy of the template
    """
    temp_dir = Path(os.getenv("TEMP", tempfile.gettempdir()))
    temp_user_scripts_path = temp_dir.joinpath("stax_test")
    temp_template_path = temp_user_scripts_path.joinpath(
        "startup/bl_app_templates_user/stax"
    )

    if not temp_template_path.exists():
        temp_template_path.mkdir(parents=True)

    # Mirror copy:
    if platform.system() == "Windows":
        copy_command = [
            "robocopy",
            repo_root.as_posix(),
            temp_template_path.as_posix(),
            "/mir",  # mirror
            "/sl",  # copy links
            # ignore some files and directories:
            "/xf",
            "*.pyc",
            "/xd",
            "__pycache__",
            "/xd",
            ".git",
            # Flags to remove robocopy's huge verbosity:
            "/NFL",
            "/NDL",
            "/NJH",
            "/NJS",
            "/nc",
            "/ns",
            "/np",
        ]
    else:
        copy_command = [
            "rsync",
            "--recursive",
            "--delete",  # mirror
            # ignore some files and directories:
            "--exclude",
            ".git",
            "--exclude",
            "__pycache__",
            "--exclude",
            "*.pyc",
            repo_root.as_posix(),
            temp_template_path.parent.as_posix(),
        ]
    subprocess.run(copy_command)

    # Make Blender see the copied app template
    os.environ["BLENDER_USER_SCRIPTS"] = temp_user_scripts_path.as_posix()

    return temp_template_path


def main():
    repo_path = Path(__file__).resolve().parent.parent.parent.parent
    app_template = setup_stax_test_template(repo_path)
    blender_load_tests_script = app_template.joinpath(
        "python", "tests", "utils", "blender_load_pytest.py"
    )
    blender_rev = "2.83.12"

    result = BAT.test_blender_addon(
        blender_exec_path=os.environ["STAX_BL_PATH"],
        app_template_path=app_template.as_posix(),
        blender_revision=blender_rev,
        config={"blender_load_tests_script": blender_load_tests_script.as_posix()},
        dir_to_ignore={".venv", ".git", "docs", "stax_dependencies"},
    )
    print("test result int ->", result)

    if result != 0:
        raise AssertionError("Tests failed")


main()
