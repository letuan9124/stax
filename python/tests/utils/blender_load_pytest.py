# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""Load pytest into blender."""

import getopt
import os
from pathlib import Path
import sys

import pytest

sys.path.append(os.environ["LOCAL_PYTHONPATH"])

# Set any value to the BLENDER_ADDON_COVERAGE_REPORTING environment variable to enable it
COVERAGE_REPORTING = os.environ.get("BLENDER_ADDON_COVERAGE_REPORTING", False)

# The Pytest tests/ path can be overriden through the BLENDER_ADDON_TESTS_PATH environment variable
TESTS_PATH = os.environ.get("BLENDER_ADDON_TESTS_PATH", "tests")

# Add explict pytest commands, just in case fine control is required
PYTEST_ARGS = os.environ.get("BLENDER_PYTEST_ARGS", "")


addon_helper = os.environ.get("ADDON_TEST_HELPER", None)
if addon_helper:
    sys.path.append(addon_helper)


class SetupPlugin:
    def __init__(self, addon="", app_template=""):
        self.addon = addon
        self.app_template = app_template


# Parse Main arguments
# ===============
options, remaining = getopt.getopt(sys.argv[1:], "b", ["app-template=", "python-expr="])

app_template = None
addon = None
for opt, arg in options:
    if opt in ("--app-template"):
        app_template = arg

# Parse custom arguments
# =====================
custom_opts, custom_remaining = getopt.getopt(remaining, "", ["addon="])
for opt, arg in custom_opts:
    if opt in ("--addon"):
        addon = arg

try:
    tests_directory = Path(__file__).parent.parent.parent.joinpath("tests")
    pytest_main_args = [
        tests_directory.as_posix(),
        "-v",
        "-x",
    ]
    if COVERAGE_REPORTING:
        pytest_main_args += ["--cov", "--cov-report", "term", "--cov-report", "xml"]
        if PYTEST_ARGS:
            pytest_main_args += [PYTEST_ARGS]
    exit_val = pytest.main(
        pytest_main_args, plugins=[SetupPlugin(app_template=app_template, addon=addon)]
    )
except Exception as e:
    print(e)
    exit_val = 1
sys.exit(exit_val)
