# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Sequencer Draw
Additional drawing into the sequencer
"""

import bpy
import blf
import bgl
import gpu
from gpu_extras.batch import batch_for_shader

from stax.utils.utils_core import get_displayed_tracks
from stax.utils.utils_timeline import get_media_sequence


def draw_sequencer():
    """Draw sequencer OpenGL"""
    # Sentinel if no sequences, abort
    if not hasattr(bpy.context.scene.sequence_editor, "sequences"):
        return

    region = bpy.context.region

    # Defining boundaries of the area to crop rectangles in view pixels' space
    x_view_min, y_view_min = region.view2d.region_to_view(0, 11)
    x_view_max, y_view_max = region.view2d.region_to_view(
        region.width - 11, region.height - 22
    )
    view_boundaries = (x_view_min, y_view_min, x_view_max, y_view_max)

    draw_track_status(view_boundaries)
    draw_channel_name(view_boundaries)


def draw_channel_name(view_boundaries):
    """Draw tracks name

    :param view_boundaries: View boundaries
    """
    region = bpy.context.region
    font_id = 0

    x_view_fixed, y_view_fixed = region.view2d.region_to_view(20, 50)
    width_track_frame_fixed = 150

    shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
    shader.bind()

    # Get tracks
    tracks = get_displayed_tracks()

    # Draw track rectangles
    bgl.glEnable(bgl.GL_BLEND)  # Enables alpha channel
    dark_track = False
    for i in range(
        0, len(tracks)
    ):  # Out of text loop because of OpenGL issue, rectangles are bugging
        shader.uniform_float("color", (0.66, 0.66, 0.66, 0.7))

        # Alternate track color
        if dark_track:
            shader.uniform_float("color", (0.5, 0.5, 0.5, 0.7))
        dark_track = not dark_track

        # Build track rectangle
        track_rectangle = get_rectangle_region_coordinates(
            (x_view_fixed, i + 1, x_view_fixed, i + 2), view_boundaries
        )

        vertices, indices = build_rectangle(
            (
                track_rectangle[0],
                track_rectangle[1],
                track_rectangle[2] + width_track_frame_fixed,
                track_rectangle[3],
            ),
            0,
        )

        # Draw the rectangle
        batch = batch_for_shader(shader, "TRIS", {"pos": vertices}, indices=indices)
        batch.draw(shader)

    # Display text
    i = 0
    for track in tracks:
        x, y = region.view2d.view_to_region(x_view_fixed, i + 1.4)

        blf.position(font_id, 30, y, 0)
        blf.size(font_id, 14, 72)
        blf.color(font_id, 1, 1, 1, 0.9)
        blf.draw(font_id, track)

        i += 1


def draw_track_status(view_boundaries):
    """Draw track's status drawn as colored rectangles underlining sequences

    :param view_boundaries: View boundaries
    """
    context = bpy.context

    # Workaround to avoid slowing from opengl draw over when the animation is playing
    if context.screen.is_animation_playing:
        return

    # Get status color
    status_colors = {
        "waiting review": [1, 0.82, 0.16, 1],
        "approved": [0.18, 0.86, 0.37, 1],
        "rejected": [0.91, 0.1, 0.1, 1],
    }

    # Draw for each strip if "status" has been set
    all_vertices = {}
    all_indices = {}

    # Init indices and vertices
    for status, _ in status_colors.items():
        all_vertices[status] = []
        all_indices[status] = []

    # Keep reviewed medias
    reviewed_sequences_coordinates = []

    for sequence in context.scene.sequence_editor.sequences:
        media_sequence = get_media_sequence(sequence)

        seq_coordinates = get_rectangle_region_coordinates(
            (
                media_sequence.frame_final_start,
                sequence.channel,
                media_sequence.frame_final_end,
                sequence.channel + 0.25,
            ),
            view_boundaries,
        )

        # Don't build if strip is out of view boundaries - Optimization
        if (
            seq_coordinates[0] == seq_coordinates[2]
            or seq_coordinates[1] == seq_coordinates[3]
        ):
            continue

        # Keep reviewed
        if media_sequence.get("reviewed"):
            reviewed_sequences_coordinates.append(seq_coordinates)

        # Append vertices and indices to statuses
        media_status = media_sequence.get("status", "waiting review")
        if media_status:
            # Get rectangles
            vertices, indices = build_rectangle(
                seq_coordinates, len(all_vertices[media_status])
            )

            all_vertices[media_status].extend(vertices)
            all_indices[media_status].extend(indices)

    # Draw the rectangles statuses by color
    for status, color in status_colors.items():
        # Set the rectangle's color
        shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        shader.bind()
        shader.uniform_float("color", color)

        # Draw
        batch = batch_for_shader(
            shader, "TRIS", {"pos": all_vertices[status]}, indices=all_indices[status]
        )
        batch.draw(shader)

    # Draw reviewed icon, a "*"
    for coordinates in reviewed_sequences_coordinates:
        x1, y1, x2, y2 = coordinates

        status_rect_height = y2 - y1
        blf.position(0, x1 + 3, y1 + status_rect_height, 0)
        height = int(status_rect_height * 3)

        # Draw only if horizontal size is greater than a value
        if x2 - x1 > height / 2:
            blf.size(0, min(height, 60), 72)  # Size clamped
            blf.color(0, 1, 1, 1, 0.8)
            blf.draw(0, "*")


def get_rectangle_region_coordinates(
    rectangle=(0, 0, 5, 5), view_boundaries=(0, 0, 1920, 1080)
):
    """Calculates strip coordinates in region's pixels norm, clamped into region

    :param rectangle: Rectangle to get coordinates of
    :param view_boundaries: Cropping area
    :return: Strip coordinates in region's pixels norm, resized for status color bar
    """
    region = bpy.context.region

    # Converts x and y in terms of the grid's frames and channels to region pixels coordinates clamped
    x1, y1 = region.view2d.view_to_region(
        max(view_boundaries[0], min(rectangle[0], view_boundaries[2])),
        max(view_boundaries[1], min(rectangle[1], view_boundaries[3])),
    )
    x2, y2 = region.view2d.view_to_region(
        max(view_boundaries[0], min(rectangle[2], view_boundaries[2])),
        max(view_boundaries[1], min(rectangle[3], view_boundaries[3])),
    )

    return x1, y1, x2, y2


def build_rectangle(coordinates, indices_offset):
    """Build an OpenGL rectangle into region

    :param coordinates: Four points coordinates
    :param indices_offset: Rectangle's indices offset
    :return: Built vertices and indices
    """
    # Building the Rectangle
    vertices = (
        (coordinates[0], coordinates[1]),
        (coordinates[2], coordinates[1]),
        (coordinates[0], coordinates[3]),
        (coordinates[2], coordinates[3]),
    )

    indices = (
        (0 + indices_offset, 1 + indices_offset, 2 + indices_offset),
        (2 + indices_offset, 1 + indices_offset, 3 + indices_offset),
    )

    return vertices, indices


def register():
    bpy.types.SpaceSequenceEditor.draw_handler_add(
        draw_sequencer, (), "WINDOW", "POST_PIXEL"
    )


def unregister():
    bpy.types.SpaceSequenceEditor.draw_handler_add(
        draw_sequencer, (), "WINDOW", "POST_PIXEL"
    )
