# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
Every ui class (panel, menu or header) relative to the general Stax's UI
"""
from pathlib import Path

import bpy
import bl_ui
from bpy.types import Header, Menu
from bpy.utils import register_class, unregister_class

import stax


class TOPBAR_HT_upper_bar(Header):
    """Top bar space header"""

    bl_space_type = "TOPBAR"

    def draw(self, context):
        layout = self.layout

        window = context.window
        screen = context.screen

        layout.separator()
        if context.region.alignment == "TOP":
            # "Stax" button to click on to display information
            box = layout.box()
            box.menu("TOPBAR_MT_stax")

            # Session menu
            layout.menu("TOPBAR_MT_session")

            layout.separator_spacer()

            # Workspaces tabs
            if not screen.show_fullscreen:
                layout.template_ID_tabs(
                    window,
                    "workspace",
                    menu="TOPBAR_MT_workspace_menu",
                )

            layout.separator_spacer()

        else:
            pass


class TOPBAR_MT_stax(Menu):
    """Stax menu"""

    bl_label = "Stax"

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        # Stax version
        layout.label(text=f"Version: {stax.version}")

        layout.separator()

        # Current loaded timeline
        if scene.timeline:
            layout.label(text=f"Timeline: {scene.timeline}")

        # # Current linked reviews
        # if scene.timeline:
        #     layout.label(text=f"Reviews:{scene.reviews}")

        layout.separator()

        # Help
        layout.label(text="Help")
        col = layout.column()
        col.operator(
            "wm.open_weburl", text="Documentation"
        ).url = "https://superprod.gitlab.io/stax_suite/stax/docs/"
        col.operator(
            "wm.open_weburl", text="Support"
        ).url = "https://discord.gg/8ZNx7Vz"
        col.operator(
            "wm.open_weburl", text="Repository"
        ).url = "https://gitlab.com/superprod/stax"

        # TODO Button to open an issue with information filled like version, system...


class TOPBAR_MT_session(Menu):
    """Session menu"""

    bl_label = "Session"

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        layout.operator_context = "INVOKE_AREA"
        layout.operator(
            "wm.read_homefile", text="New", icon="FILE_NEW"
        ).app_template = context.preferences.app_template

        layout.separator()

        # Update timeline
        # If timeline cached and scene is Main, display appropriate UI
        if scene.is_timeline_loaded and scene is bpy.data.scenes["Scene"]:
            # Update current timeline
            row = layout.row()
            row.operator("sequencer.update_session", text="Update", icon="FILE_REFRESH")

            # Disable Update button when review session is active to avoid session reset
            row.enabled = not context.scene.review_session_active

        layout.separator()

        # Load timeline
        layout.operator("sequencer.load_timeline", icon="ASSET_MANAGER")
        layout.menu("TOPBAR_MT_session_reviews")

        layout.separator()

        layout.operator_context = (
            "EXEC_AREA" if context.blend_data.is_saved else "INVOKE_AREA"
        )
        layout.operator("wm.save_mainfile", text="Save", icon="FILE_TICK")

        layout.separator()

        layout.operator("wm.quit_blender", text="Quit", icon="QUIT")


class TOPBAR_MT_session_reviews(Menu):
    """Session Reviews menu"""

    bl_label = "Reviews"

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        # Link reviews, displayed if sequences
        if scene.timeline or scene.sequence_editor.sequences:
            layout.operator("scene.link_reviews", icon="DOCUMENTS")

        layout.separator()

        # Publish/Abort
        layout.operator("scene.abort_reviews", icon="CANCEL")
        layout.operator("scene.publish_reviews", icon="CHECKMARK")


classes = [
    TOPBAR_HT_upper_bar,
    TOPBAR_MT_stax,
    TOPBAR_MT_session,
    TOPBAR_MT_session_reviews,
]


def register():
    # Set Custom UI
    for cls in classes:
        register_class(cls)


def unregister():
    # Clear Custom UI
    for cls in classes:
        unregister_class(cls)
