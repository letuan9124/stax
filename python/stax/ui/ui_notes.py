# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

from pathlib import Path

import bpy
from bpy.utils import register_class, unregister_class

import stax
from stax.utils.utils_timeline import get_media_sequence
from stax.utils.utils_ui import (
    display_note,
    display_pending_note,
    display_pending_reply,
    get_stax_icon,
)


class NOTES_PT_pending_text_note(bpy.types.Panel):
    """Pending text note panel"""

    bl_label = "Pending note"
    bl_region_type = "UI"
    bl_category = "Notes"

    def draw_header(self, context):
        layout = self.layout

        layout.operator(
            "wm.write_comment",
            icon="ADD",
            text="",
        )

    def draw(self, context):
        layout = self.layout
        scene = bpy.data.scenes["Scene"]
        media_sequence = get_media_sequence(scene.sequence_editor.active_strip)

        tool_shelf = None
        area = context.area

        for region in area.regions:
            if region.type == "UI":
                tool_shelf = region

        width = tool_shelf.width / 8

        # Display pending note
        display_pending_note(
            layout,
            width,
            scene.user_preferences.user_login,
            media_sequence,
        )

        # Write new text comment
        write_comment_icon = get_stax_icon("write_comment")
        layout.operator(
            "wm.write_comment",
            icon_value=write_comment_icon.icon_id,
            text="New comment",
        )


class NOTES_PT_text_notes(bpy.types.Panel):
    """All current media text notes panel"""

    bl_label = "Text notes"
    bl_region_type = "UI"
    bl_category = "Notes"

    def draw_header(self, context):
        layout = self.layout
        wm = context.window_manager

        # Add comment button
        NOTES_PT_pending_text_note.draw_header(self, context)

        layout.prop(
            wm,
            "newer_to_older",
            icon="TRIA_UP" if wm.newer_to_older else "TRIA_DOWN",
            text="",
        )

    def draw(self, context):
        layout = self.layout
        wm = context.window_manager
        scene = bpy.data.scenes["Scene"]
        media_sequence = get_media_sequence(scene.sequence_editor.active_strip)
        review = scene.reviews.get(media_sequence.get("review_name", ""))

        # TODO check if it's possible to make it common with other occurrences
        tool_shelf = None
        area = context.area

        for region in area.regions:
            if region.type == "UI":
                tool_shelf = region

        width = tool_shelf.width / 8

        # Sentinel to not try to display comments is there is no any review associated to the media
        if review:
            notes_to_display = (
                reversed(review.notes) if wm.newer_to_older else review.notes
            )
            for stax_note in notes_to_display:
                # Sentinel to avoid displaying twice replies as usual comments
                # and textless notes
                if stax_note.parent or (
                    not stax_note.text_comments and not stax_note.text_annotations
                ):
                    continue

                box = display_note(layout, width, stax_note) or layout.box()

                box.separator_spacer()

                # Replies
                col = box.row()
                split = col.split(factor=0.05)
                left = split.column()

                right = split.column()

                for reply in stax_note.replies:
                    # Get the object by the reference's name
                    reply = review.notes.get(reply.name)

                    display_note(right, width / 1.05, reply)

                # Display pending reply
                textpad_name = f"REPLY_{stax_note.name}"
                pending_reply_box = display_pending_reply(
                    right,
                    width,
                    scene.user_preferences.user_login,
                    None,
                    textpad_name,
                )

                # Reply Button
                footer = box.row()
                ops = footer.operator(
                    "wm.reply_to_note",
                    text="Edit Reply" if pending_reply_box else "Reply",
                )
                ops.review_name = review.name
                ops.note_name = stax_note.name

        # Display pending note
        display_pending_note(
            layout,
            width,
            scene.user_preferences.user_login,
            media_sequence,
        )

        # Button Write new text comment
        write_comment_icon = get_stax_icon("write_comment")
        layout.operator(
            "wm.write_comment",
            icon_value=write_comment_icon.icon_id,
            text="New comment",
        )
