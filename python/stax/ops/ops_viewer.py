# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
import uuid

import bpy

from stax.utils.utils_core import get_collection_entity, get_context, get_text_body
from stax.utils.utils_timeline import (
    append_to_object_key,
    get_current_sequence,
    get_media_sequence,
    set_reviewed_state,
)
from stax.utils.utils_ui import open_text_window


class TEXT_OT_write_comment(bpy.types.Operator):
    """Allows you to create a comment for the current media"""

    bl_idname = "wm.write_comment"
    bl_label = "Write Text Comment"

    confirm: bpy.props.BoolProperty(options={"SKIP_SAVE"})
    edit: bpy.props.BoolProperty(options={"SKIP_SAVE"})

    @classmethod
    def poll(cls, context):
        media_sequence = get_media_sequence(
            bpy.data.scenes["Scene"].sequence_editor.active_strip
        )
        if media_sequence and not media_sequence.lock:
            return True

    def execute(self, context):
        scene = context.scene

        # Get current sequence
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        media_sequence = get_media_sequence(current_sequence)

        if self.confirm:  # Confirm the comment
            # Test if the content is empty, then delete the textpad
            textpad = context.space_data.text

            text_body = get_text_body(textpad)
            if not text_body:
                bpy.data.texts.remove(textpad)
            else:
                # It's a whole new note, not a reply
                if not textpad.name.startswith("REPLY_"):
                    # Define if it is an annotation or a comment
                    # Text Annotation: The preview range is still active AND preview range start OR end values are contained into the sequence boundaries
                    # Text Comment: Any other case
                    if scene.use_preview_range and (  # Text annotation
                        scene.frame_preview_start > media_sequence.frame_start
                        or scene.frame_preview_end < media_sequence.frame_final_end - 1
                    ):
                        # Set as annotation
                        annotation_frame_start = (
                            max(
                                scene.frame_preview_start,
                                current_sequence.frame_final_start,
                                current_sequence.frame_final_start,
                            )
                            - media_sequence.frame_start
                        )
                        annotation_frame_end = (
                            min(
                                scene.frame_preview_end,
                                current_sequence.frame_final_end - 1,
                                media_sequence.frame_final_end - 1,
                            )
                            - media_sequence.frame_start
                        )

                        # Rename blender text object and associate data
                        textpad.name = (
                            f"ANNOTATION_{media_sequence.name}_{str(uuid.uuid4())[:8]}"
                        )
                        textpad["frame_start"] = annotation_frame_start
                        textpad["frame_end"] = annotation_frame_end
                    else:  # Text comment
                        textpad.name = f"{textpad.name.replace('TEXT_', 'COMMENT_')}_{str(uuid.uuid4())[:8]}"

                    # Associate to sequence
                    text_annotations = media_sequence.get("text_contents", [])
                    if textpad not in text_annotations:
                        append_to_object_key(media_sequence, "text_contents", textpad)

            # Clear preview range
            scene.use_preview_range = False

            # Close the window
            bpy.ops.wm.window_close()

            # Set as reviewed
            set_reviewed_state(media_sequence)

        else:  # Open the text window
            # Open text window
            text_name = f"TEXT_{media_sequence.name}"
            text_pad = get_collection_entity(bpy.data.texts, text_name)
            open_text_window(text_pad)

            # Open the comments thread if the space allows it
            if context.space_data:
                context.space_data.show_region_ui = True

            # Set the preview range around the sequence
            scene.use_preview_range = True
            scene.frame_preview_start, scene.frame_preview_end = (
                current_sequence.frame_final_start,
                current_sequence.frame_final_end - 1,
            )

            # Frame the sequence
            bpy.ops.sequencer.select_all(action="DESELECT")
            current_sequence.select = True
            bpy.ops.sequencer.view_selected(get_context("Main", "SEQUENCER"))

            # Start review session
            context.scene.review_session_active = True

        return {"FINISHED"}


class TEXT_OT_edit_comment(bpy.types.Operator):
    """Allows you to edit a comment"""

    bl_idname = "wm.edit_comment"
    bl_label = "Edit Text Comment"

    text_name: bpy.props.StringProperty(name="Text object name to edit")
    move_playhead: bpy.props.BoolProperty(options={"SKIP_SAVE"}, default=True)

    @classmethod
    def poll(cls, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if current_sequence and not current_sequence.lock:
            return True

    def execute(self, context):
        text_pad = bpy.data.texts.get(self.text_name)

        # Get current sequence
        media_sequence = get_media_sequence(
            bpy.data.scenes["Scene"].sequence_editor.active_strip
        )

        # Show annotation
        frame_start = text_pad.get("frame_start", 0) + media_sequence.frame_start
        frame_end = (
            text_pad.get("frame_end", media_sequence.frame_final_duration)
            + media_sequence.frame_start
        )
        bpy.ops.sequencer.show_text_annotation(
            frame_start=frame_start,
            frame_end=frame_end,
            set_preview_range=True,
            move_playhead=self.move_playhead,
        )

        # Load text in the editor
        open_text_window(text_pad)

        return {"FINISHED"}


class TEXT_OT_reply_to_note(bpy.types.Operator):
    """Create or edit a reply to the comment"""

    bl_idname = "wm.reply_to_note"
    bl_label = "Reply"

    review_name: bpy.props.StringProperty()
    note_name: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if current_sequence and not current_sequence.lock:
            return True

    def execute(self, context):
        # Load text in the editor
        text_name = f"REPLY_{self.note_name}"
        text_pad = get_collection_entity(bpy.data.texts, text_name)
        open_text_window(text_pad)

        # Set data to text object
        text = bpy.data.texts.get(text_name)
        text["review_name"] = self.review_name
        text["note_name"] = self.note_name

        return {"FINISHED"}


class TEXT_OT_delete_comment(bpy.types.Operator):
    """Delete the current text comment"""

    bl_idname = "wm.delete_comment"
    bl_label = "Delete Comment"

    textpad_name: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if current_sequence and not current_sequence.lock:
            return True

    def execute(self, context):
        text = bpy.data.texts.get(self.textpad_name)
        media_sequence = get_media_sequence(
            bpy.data.scenes["Scene"].sequence_editor.active_strip
        )

        # Delete comment text
        bpy.data.texts.remove(text)

        # Clean sequence of the text to delete
        contents = media_sequence.get("text_contents", [])
        if contents:
            media_sequence["text_contents"] = [
                a for a in media_sequence.get("text_contents") if a
            ]

        # Clear Preview range
        context.scene.use_preview_range = False

        # Close window
        if hasattr(context.space_data, "text"):
            bpy.ops.wm.window_close()

        # Set reviewed if not
        set_reviewed_state(media_sequence)

        return {"FINISHED"}


classes = [
    TEXT_OT_delete_comment,
    TEXT_OT_reply_to_note,
    TEXT_OT_write_comment,
    TEXT_OT_edit_comment,
]


def register():

    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
