# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to sequencer UI
"""

import bpy
from bpy.types import MetaSequence, Sequence
from openreviewio import MediaReview

from stax.utils.utils_core import get_selected_sequences
from stax.utils.utils_timeline import get_media_sequence
from stax.properties.properties_core import Note
from stax.utils.utils_timeline import add_overlay_to_sequence, display_media_annotations


class WM_OT_split_view(bpy.types.Operator):
    """Toggle Split sequencer view"""

    bl_idname = "wm.splitview"
    bl_label = "Split View"

    def invoke(self, context, event):
        if event.shift:
            context.window.screen = bpy.data.screens["Scripting"]
            return {"FINISHED"}

        sq_preview_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ]
        if len(sq_preview_areas) == 1:
            # Split area
            bpy.ops.screen.area_split(
                {"area": sq_preview_areas[0]}, direction="VERTICAL"
            )

            # Disable toolbar on right panel
            # Update areas
            sq_preview_areas = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
            ]

            # Hide toolbar
            sq_preview_areas[0].spaces[0].show_region_toolbar = False

        else:
            # Give the remaining area to have the left area's channel displayed
            sq_preview_areas[0].spaces[0].display_channel = (
                sq_preview_areas[-1].spaces[0].display_channel
            )

            # Show toolbar of remaning area
            sq_preview_areas[0].spaces[0].show_region_toolbar = True

            # Join areas
            # Simulates the mouse position as being between the two areas horizontally
            # and a bit above the bottom corner vertically
            cursor_x = int(
                sq_preview_areas[0].x
                - (sq_preview_areas[0].x - sq_preview_areas[1].width) / 2
            )
            cursor_y = sq_preview_areas[0].y + 10
            bpy.ops.screen.area_join(
                cursor=(
                    cursor_x,
                    cursor_y,
                )
            )

            # Force UI update, due to Blender bug, delete when fixed -> https://developer.blender.org/T65529
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )

        return {"FINISHED"}


class WM_OT_mouse_location(bpy.types.Operator):
    """This operator shows the mouse location when left clicking

    TODO Maybe create a ops_dev.py to store development utils

    For debug only. Run it with the operator search function F3"""

    bl_idname = "wm.mouse_position"
    bl_label = "Mouse location"

    def modal(self, context, event):
        if event.type == "LEFTMOUSE":
            print(f"x: {event.mouse_x}, y: {event.mouse_y}")
        return {"PASS_THROUGH"}

    def execute(self, context):
        context.window_manager.modal_handler_add(self)
        return {"RUNNING_MODAL"}


class WM_OT_DetachView(bpy.types.Operator):
    """Create a detached window of the current sequencer view"""

    bl_idname = "wm.detachview"
    bl_label = "Detach View"

    def execute(self, context):
        sq_preview_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ]
        if sq_preview_areas:
            bpy.ops.screen.area_dupli(
                {"area": sq_preview_areas[-1]}, "INVOKE_DEFAULT"
            )  # TODO : unexpected arg ?
        return {"FINISHED"}


class SEQUENCER_OT_ShowAnnotations(bpy.types.Operator):
    """Show annotations of selected sequences"""

    bl_idname = "sequencer.show_sequences_annotations"
    bl_label = "Show Sequences Annotations"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        scene = context.scene

        # Sentinel
        if not context.scene.reviews:
            self.report(type={"INFO"}, message="No reviews to get annotations from.")

        if not context.scene.all_annotations_displayed:
            # Select all sequences
            bpy.ops.sequencer.select_all(
                {"scene": bpy.data.scenes["Scene"]}, action="SELECT"
            )

            # Unlock
            bpy.ops.sequencer.unlock({"scene": bpy.data.scenes["Scene"]})

            # Display annotations
            self.execute(context)

            # Lock back
            bpy.ops.sequencer.lock_review()

            # Highlight updated reviews
            bpy.ops.sequencer.highlight_updated_reviews(
                {"scene": bpy.data.scenes["Scene"]}
            )

            # Toggle annotate tool drawings
            context.scene.all_annotations_displayed = True

            # Update active parameters
            scene.frame_set(scene.frame_current)

        return {"FINISHED"}

    def execute(self, context):
        scene = context.scene
        any_annotation_displayed = False

        # Sentinel
        if not scene.reviews:
            return {"CANCELLED"}

        # Build annotations
        # ------------------
        selected_sequences = get_selected_sequences()
        for sequence in selected_sequences:
            media_sequence = get_media_sequence(sequence)

            #   Display image annotations of sequence
            stax_review = scene.reviews.get(media_sequence.get("review_name", ""))

            # Continue if there is no review linked to the media
            if not stax_review:
                continue

            # Dynamic function
            def display_stax_notes(
                parent_meta: MetaSequence, stax_note
            ) -> MetaSequence:
                """Create note as metasequence above given sequence

                :param parent_meta: Meta sequence in which the overlay will be created
                :param stax_note: Note to add as an overlay
                :return: Created metasequence
                """
                scene = bpy.context.scene
                # If the note doesn't already exists create image annotations
                note_sequence = parent_meta.sequences.get(stax_note.date)
                if not note_sequence:
                    created_annotations = []
                    for i, annotation in enumerate(stax_note.image_annotations):
                        # Calculate the frame in of the image,
                        # strip if started before start of clip
                        annotation_start_frame = (
                            media_sequence.frame_start + annotation.frame
                        )
                        if annotation_start_frame < media_sequence.frame_final_start:
                            frame_end = annotation_start_frame + annotation.duration
                            if frame_end > media_sequence.frame_final_start:
                                annotation_start_frame = (
                                    media_sequence.frame_final_start
                                )
                            else:  # In the case that the annotation ends before clip start, do not create annotation
                                continue

                        # Create image sequence
                        image = scene.sequence_editor.sequences.new_image(
                            name=annotation.name,
                            filepath=f"{annotation.path_to_image}",
                            channel=media_sequence.channel + i,
                            frame_start=annotation_start_frame,
                        )
                        if image:
                            image.frame_final_duration = annotation.duration
                            image.blend_type = "ALPHA_OVER"
                            image["kind"] = "annotation"
                            created_annotations.append(image)

                    # Metastrip annotations from note
                    if created_annotations:
                        bpy.ops.sequencer.select_all(
                            {"scene": bpy.data.scenes["Scene"]}, action="DESELECT"
                        )
                        for a in created_annotations:
                            a.select = True
                        bpy.ops.sequencer.meta_make({"scene": bpy.data.scenes["Scene"]})
                        created_meta = scene.sequence_editor.active_strip
                        created_meta.name = stax_note.date
                        created_meta.blend_type = "ALPHA_OVER"
                        created_meta["kind"] = "note"

                        return created_meta

            # Create overlay for every note if has image annotations
            for stax_note in stax_review.notes:
                if stax_note.image_annotations:
                    created_note = add_overlay_to_sequence(
                        media_sequence, (display_stax_notes, {"stax_note": stax_note})
                    )

                    if created_note:
                        any_annotation_displayed = True
            # --------------

            # Unmute annotation related metasequences
            if not media_sequence.get("annotations_displayed", False):
                media_annotations_displayed = display_media_annotations(
                    media_sequence, True
                )

                # Keep the information
                media_sequence["annotations_displayed"] = media_annotations_displayed
                any_annotation_displayed = (
                    media_annotations_displayed or any_annotation_displayed
                )

        # Refresh the sequencer TODO: probably won't be needed after this fix: https://developer.blender.org/T79922
        bpy.ops.sequencer.refresh_all()

        return {"FINISHED"}


class SEQUENCER_OT_HideAnnotations(bpy.types.Operator):
    """Hide annotations of selected sequences"""

    bl_idname = "sequencer.hide_sequences_annotations"
    bl_label = "Hide Sequences Annotations"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        if context.scene.all_annotations_displayed:
            scene = context.scene

            # Select all sequences
            bpy.ops.sequencer.select_all(action="SELECT")

            # Unlock
            bpy.ops.sequencer.unlock()

            # Hide annotations
            self.execute(context)

            # Lock back
            bpy.ops.sequencer.lock_review()

            # Highlight updated reviews
            bpy.ops.sequencer.highlight_updated_reviews(
                {"scene": bpy.data.scenes["Scene"]}
            )

            context.scene.all_annotations_displayed = False

            # Update active parameters
            scene.frame_set(scene.frame_current)

        return {"FINISHED"}

    def execute(self, context):
        current_sequences = get_selected_sequences()
        for sequence in current_sequences:
            media_sequence = get_media_sequence(sequence)
            # Mute annotation related metasequences
            if media_sequence.get("annotations_displayed", False):
                display_media_annotations(media_sequence, False)

                media_sequence["annotations_displayed"] = False

        # Refresh the sequencer TODO: probably won't be needed after this fix: https://developer.blender.org/T79922
        bpy.ops.sequencer.refresh_all()

        return {"FINISHED"}


class SEQUENCER_OT_toggle_annotations_keyframes(bpy.types.Operator):
    """Toggle annotations keyframes editing"""

    bl_idname = "sequencer.toggle_annotations_keyframes"
    bl_label = "Toggle Annotations Keyframes"
    bl_options = {"REGISTER", "UNDO"}

    # TODO make it simplier with https://gitlab.com/superprod/stax/-/issues/154

    @classmethod
    def poll(cls, context):
        if context.workspace is bpy.data.workspaces["Main"]:
            return True

    def execute(self, context):
        sq_sequencer_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
        ]
        sq_dopesheet_areas = [
            x
            for x in context.screen.areas
            if x.type == "DOPESHEET_EDITOR" and x.spaces[0].ui_mode == "GPENCIL"
        ]
        if not sq_dopesheet_areas:
            # Split area
            bpy.ops.screen.area_split(
                {"area": sq_sequencer_areas[0]}, direction="HORIZONTAL"
            )

            # Update and get created areas
            sq_sequencer_areas = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
            ]

            # Change to display keyframes
            keyframes_area = sq_sequencer_areas[0]
            keyframes_area.type = "DOPESHEET_EDITOR"

        else:
            # Join areas
            # Simulates the mouse position as being between the two areas vertically
            # and a bit above the bottom corner horizontally
            cursor_x = int(sq_dopesheet_areas[-1].width / 2)
            cursor_y = int(sq_dopesheet_areas[-1].y - 3)
            bpy.ops.screen.area_join(
                cursor=(
                    cursor_x,
                    cursor_y,
                )
            )

            # Force UI update, due to Blender bug, delete when fixed -> https://developer.blender.org/T65529
            sq_preview_areas = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
            ]
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )

            # Set to sequencer back, else the dopesheet remains
            sq_dopesheet_areas = [
                x
                for x in context.screen.areas
                if x.type == "DOPESHEET_EDITOR" and x.spaces[0].ui_mode == "GPENCIL"
            ]
            sq_dopesheet_areas[0].type = "SEQUENCE_EDITOR"

            # Move the tool header to top back, else it stays on the bottom by default
            sq_sequencer_areas = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
            ]

            bpy.ops.screen.region_flip(
                {
                    "area": sq_sequencer_areas[0],
                    "region": sq_sequencer_areas[0].regions[0],
                }
            )

        return {"FINISHED"}


classes = [
    WM_OT_mouse_location,
    WM_OT_split_view,
    WM_OT_DetachView,
    SEQUENCER_OT_ShowAnnotations,
    SEQUENCER_OT_HideAnnotations,
    SEQUENCER_OT_toggle_annotations_keyframes,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
