# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to timeline
"""
from datetime import datetime, timezone


import bpy
from bpy.props import StringProperty
from bpy_extras.io_utils import ImportHelper


import opentimelineio as otio

from stax.utils import (
    utils_cache,
    utils_core,
    utils_ui,
)

from stax.utils.utils_cache import (
    save_session,
)
from stax.utils.utils_core import (
    get_context,
    get_selected_sequences,
)
from stax.utils.utils_timeline import (
    set_preview_range_from_sequences,
    frame_change_update,
    load_timeline,
)
from stax.properties.properties_core import Track


class LoadTimeline(bpy.types.Operator, ImportHelper):
    """Load timeline"""

    bl_idname = "sequencer.load_timeline"
    bl_label = "Load timeline"

    filter_glob: StringProperty(
        default=f"*.{';*.'.join(otio.adapters.suffixes_with_defined_adapters())}",
        options={"HIDDEN"},
    )

    filepath: StringProperty(
        options={"HIDDEN"},
    )

    def execute(self, context):
        # Display loading for user
        if context.window:
            context.window.cursor_set("WAIT")

        load_timeline(self.filepath)

        return {"FINISHED"}


class STAX_OT_update_session(bpy.types.Operator):
    """Update the current session\nSHIFT + click reset the timeline cache"""

    bl_idname = "sequencer.update_session"
    bl_label = "Update"

    reset_tracks = None

    def invoke(self, context, event):

        # Display loading for user
        if context.window:
            context.window.cursor_set("WAIT")

        # Reset cache
        if event.shift:
            self.reset_tracks = utils_core.get_displayed_tracks()
            timeline = context.scene.timeline

            # Also delete the cached media
            utils_cache.delete_session_cache(timeline, full_reset=event.ctrl)

            # Use default load timeline operator
            bpy.ops.sequencer.load_timeline(filepath=timeline)

            return {"FINISHED"}

        self.execute(context)
        return {"FINISHED"}

    def execute(self, context):
        scene = context.scene

        # Keep update date
        update_date = datetime.now(timezone.utc).isoformat()

        # Load timeline
        bpy.ops.sequencer.load_timeline(filepath=scene.timeline)

        # Update scene
        scene = bpy.context.scene

        # Frame view to all sequences in the timeline
        override = get_context("Main", "SEQUENCER")
        bpy.ops.sequencer.view_all(override)

        # Set all reviews to not updated
        for stax_review in scene.reviews:
            stax_review.updated = False

        # Link reviews
        bpy.ops.scene.link_reviews(
            override,
            directory=scene.reviews_linked_dir,
        )

        # Highlight updated reviews
        bpy.ops.sequencer.highlight_updated_reviews({"scene": bpy.data.scenes["Scene"]})

        # Save date
        context.scene.stax_info.session_last_update = update_date

        # Save session
        save_session()

        return {"FINISHED"}


class SEQUENCER_OT_LockReview(bpy.types.Operator):
    """Lock sequences depending on their "review_enabled" parameter"""

    bl_idname = "sequencer.lock_review"
    bl_label = "Lock Review Disabled Sequences"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def execute(self, context):
        # Select all sequences
        override = {"scene": bpy.data.scenes["Scene"]}
        bpy.ops.sequencer.select_all(override, action="SELECT")
        selected_sequences = get_selected_sequences()

        for seq in selected_sequences:
            seq.lock = not seq.get("review_enabled", True)

        # Deselect
        bpy.ops.sequencer.select_all(override, action="DESELECT")

        return {"FINISHED"}


class SEQUENCER_OT_HighlightUpdatedReviews(bpy.types.Operator):
    """Highlight sequences depending on the 'updated' parameter of their related review by selecting them"""

    bl_idname = "sequencer.highlight_updated_reviews"
    bl_label = "Highlight Updated Reviews"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        # Sentinel
        if not context.scene.reviews:
            self.report(type={"WARNING"}, message="No any loaded reviews.")
            return {"CANCELLED"}

    def execute(self, context):
        scene = context.scene

        # Sentinel
        if not scene.reviews:
            return {"CANCELLED"}

        # Deselect all sequences
        bpy.ops.sequencer.select_all(
            {"scene": bpy.data.scenes["Scene"]}, action="DESELECT"
        )

        # Select sequences
        for sequence in scene.sequence_editor.sequences:
            sequence.select = sequence.get("updated", False)

        return {"FINISHED"}


class SEQUENCER_OT_set_preview_range_selected_sequences(bpy.types.Operator):
    """Set the preview range to the selected sequences"""

    bl_idname = "sequencer.set_preview_range_selected_sequences"
    bl_label = "Set Preview Range Selected Sequences"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def execute(self, context):
        # Set preview range
        set_preview_range_from_sequences(context.selected_sequences)

        # Deselect all
        bpy.ops.sequencer.select_all(action="DESELECT")

        # Set current displayed sequence as active
        frame_change_update(context.scene)

        return {"FINISHED"}


class SEQUENCER_OT_offset_preview_display_channel(bpy.types.Operator):
    """Change preview display channel by an offset"""

    bl_idname = "sequencer.offset_preview_display_channel"
    bl_label = "Offset Preview Display Channel"

    channel_offset: bpy.props.IntProperty(name="Channel to display", default=1)

    def execute(self, context):
        scene = context.scene
        preview_area = get_context("Main", "PREVIEW").get("space_data")

        # Apply offset
        target_track_index = preview_area.display_channel - 1 + self.channel_offset

        # Make the selection loop
        if target_track_index >= len(scene.tracks):
            target_track_index = 0
        elif target_track_index < 0:
            target_track_index = len(scene.tracks) - 1

        # Change current displayed track
        scene.current_displayed_track = scene.tracks[target_track_index].name

        return {"FINISHED"}


class SEQUENCER_OT_show_text_annotation(bpy.types.Operator):
    """Click to move the playhead to first frame of the annotation.\nShift + Click to set the preview range"""

    bl_idname = "sequencer.show_text_annotation"
    bl_label = "Show Text Annotation"

    frame_start: bpy.props.IntProperty(name="Start frame", options={"SKIP_SAVE"})
    frame_end: bpy.props.IntProperty(name="End frame", options={"SKIP_SAVE"})
    set_preview_range: bpy.props.BoolProperty(options={"SKIP_SAVE"})
    move_playhead: bpy.props.BoolProperty(options={"SKIP_SAVE"}, default=True)

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        scene = context.scene

        # Shift click on a text annotation to set the preview range
        if event.shift:
            # If preview range already set and matching the current annotation, disable it, else set the new one
            if scene.use_preview_range and (
                scene.frame_preview_start == self.frame_start
                and scene.frame_preview_end == self.frame_end
            ):
                scene.use_preview_range = False
            else:
                self.set_preview_range = True

        self.execute(context)

        return {"FINISHED"}

    def execute(self, context):
        scene = context.scene
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip

        # Move playhead to starting frame of annotation
        if self.move_playhead:
            scene.frame_set(self.frame_start)

        # Set annotation preview range button only if wider than 1
        # (avoid 1 frame at sequence's start when annotation is out of boundaries because of handles)
        if self.set_preview_range and self.frame_end - self.frame_start > 1:
            scene.use_preview_range = True
            scene.frame_preview_start, scene.frame_preview_end = (
                self.frame_start,
                min(self.frame_end, current_sequence.frame_final_end),
            )

        return {"FINISHED"}


class SEQUENCER_OT_set_current_frame_only(bpy.types.Operator):
    """Set the range to the current frame"""

    bl_idname = "sequencer.set_current_frame_only"
    bl_label = "Set to Current Frame"

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.sequences:
            return True

    def execute(self, context):
        scene = context.scene

        # Set preview range to current frame
        scene.use_preview_range = True
        scene.frame_preview_start, scene.frame_preview_end = (
            scene.frame_current,
            scene.frame_current,
        )

        return {"FINISHED"}


classes = [
    # UpdateTimeline,
    LoadTimeline,
    STAX_OT_update_session,
    SEQUENCER_OT_LockReview,
    SEQUENCER_OT_HighlightUpdatedReviews,
    SEQUENCER_OT_set_preview_range_selected_sequences,
    SEQUENCER_OT_offset_preview_display_channel,
    SEQUENCER_OT_show_text_annotation,
    SEQUENCER_OT_set_current_frame_only,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.timeline = bpy.props.StringProperty(
        name="Source timeline",
        description="The session's editing is built from this timeline",
    )


def unregister():
    del bpy.types.Scene.timeline

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
