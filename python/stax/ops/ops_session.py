# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to user session
"""
from pathlib import Path
import shutil
from uuid import uuid4
import os

import bpy
from bpy.props import StringProperty
from bpy.types import Operator
import openreviewio as orio

import stax
from stax import Callbacks
from stax.utils import (
    utils_cache,
    utils_config,
    utils_core,
    utils_linker,
    utils_timeline,
)
from stax.utils.utils_core import get_collection_entity, get_context, get_text_contents
from stax.utils.utils_cache import (
    get_orio_default_cache_directory,
    get_temp_directory,
    save_session,
)
from stax.utils.utils_note import (
    clear_advanced_drawings,
    clear_annotate,
    clear_text_content,
)
from stax.utils.utils_timeline import (
    get_media_sequence,
    get_parent_metas,
    get_source_media_path,
    get_visible_sequences,
    set_sequence_attributes,
)


class SCENE_OT_ChangeStaxConfiguration(bpy.types.Operator):
    """Open the Stax configuration panel"""

    bl_idname = "scene.change_stax_configuration"
    bl_label = "User configuration"

    save_configuration: bpy.props.BoolProperty(
        name="Save Configuration",
        default=False,
        description="Save User configuration boolean",
    )

    def invoke(self, context, event):
        if context.window.workspace is bpy.data.workspaces["Stax configuration"]:
            # Save configuration
            self.execute(context)
        else:
            # Go to User Configuration workspace
            context.window_manager.source_workspace = context.window.workspace.name
            context.window.workspace = bpy.data.workspaces["Stax configuration"]

        return {"FINISHED"}

    def execute(self, context):
        if self.save_configuration:
            # Save Blender preferences
            bpy.ops.wm.save_userpref()

            # Write changes in cache user config
            utils_config.write_user_config()

        # Go to previous workspace if went from there, else go to player mode
        if context.window_manager.source_workspace == "Stax configuration":
            context.window.workspace = bpy.data.workspaces["Main"]
        else:
            context.window.workspace = bpy.data.workspaces[
                context.window_manager.source_workspace
            ]

        return {"FINISHED"}


def switch_save_credentials(self, context):
    """Set auto_login to False if save_credentials is False"""

    if not self.save_credentials:
        self.auto_login = False


def switch_auto_login(self, context):
    """Set save_credentials to True if auto_login is True"""

    if self.auto_login:
        self.save_credentials = True


class WM_OT_ProductionManagerAuthentication(bpy.types.Operator):
    """Log-in to synchronize Stax with Production Manager"""

    bl_idname = "wm.pm_authentication"
    bl_label = "Production Manager authentication"

    login: bpy.props.StringProperty(name="User")
    password: bpy.props.StringProperty(name="Password", subtype="PASSWORD")
    save_credentials: bpy.props.BoolProperty(
        name="Save Credentials", update=switch_save_credentials
    )
    auto_login: bpy.props.BoolProperty(name="Auto Log In", update=switch_auto_login)

    def invoke(self, context, event):
        user_prefs = context.scene.user_preferences

        # Initialization from user preferences
        self.login = user_prefs.user_login
        self.save_credentials = user_prefs.user_save_credentials

        if self.save_credentials:
            self.password = user_prefs.user_password
            self.auto_login = user_prefs.user_auto_login
        else:
            self.password = ""

        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):
        user_preferences = context.scene.user_preferences

        # Display loading for user
        context.window.cursor_set("WAIT")

        # Update user preferences
        user_preferences.user_login = self.login
        user_preferences.user_auto_login = self.auto_login
        user_preferences.user_save_credentials = self.save_credentials

        #   If logged in, save password and write config
        #   Else, set password to empty
        linker = utils_linker.get_linker()
        if linker and linker.authentication_exec(self.login, self.password):
            # Update user preferences
            user_preferences.user_password = self.password
            user_preferences.user_save_credentials = self.save_credentials

            # Write config
            utils_config.write_user_config()

            # Load last timeline # TODO
            bpy.types.Scene.session_logged_in = True
            bpy.ops.scene.load_previous_session()

            self.report({"INFO"}, "Authentication Successful")
        else:
            user_preferences.user_password = ""

            bpy.types.Scene.session_logged_in = False

            self.report({"ERROR"}, "Authentication failed")

        return {"FINISHED"}


class LoadPreviousSession(bpy.types.Operator):
    """Load previous session at Log-in"""

    bl_idname = "scene.load_previous_session"
    bl_label = "Load previous user's session"

    def execute(self, context):
        user_preferences = context.scene.user_preferences

        if user_preferences.logging_auto_update:
            utils_timeline.timeline_load(
                user_preferences.project_name,
                user_preferences.timeline_name,
                self.report,
                True,
            )

        return {"FINISHED"}


class SCENE_OT_link_reviews(bpy.types.Operator):
    """Link reviews to the session's timeline"""

    bl_idname = "scene.link_reviews"
    bl_label = "Link Reviews"

    filter_glob: StringProperty(
        default="*.orio",
        options={"HIDDEN"},
    )

    directory: StringProperty(
        name="Reviews directory", subtype="DIR_PATH", options={"HIDDEN"}
    )

    copy_to_cache: bpy.props.BoolProperty(
        name="Copy reviews to cache",
        description="Copy the reviews from the target directory to Stax's cache",
    )

    @classmethod
    def poll(cls, context):
        scene = context.scene
        return scene.timeline != "" or scene.sequence_editor.sequences

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {"RUNNING_MODAL"}

    def execute(self, context):
        scene = context.scene
        reviews_dir = Path(self.directory)

        override = get_context("Main", "SEQUENCER")

        # Tell to wait
        if hasattr(context, "window") and context.window:
            context.window.cursor_set("WAIT")

        # Clear session
        # -------------
        # Clear only if source directory is different from the targeted one
        # Else it's only updating
        if scene.reviews_linked_dir != self.directory:
            # Hide annotations
            bpy.ops.sequencer.hide_sequences_annotations(override, "INVOKE_DEFAULT")
            # Clear existing reviews
            scene.reviews.clear()

        # Load reviews
        # ------------
        # Import reviews in session
        review_imported = False
        for child in reviews_dir.glob("*.orio"):
            orio_review = orio.load_media_review(child)
            stax_review = get_collection_entity(
                scene.reviews, str(child.joinpath(orio_review.media).resolve())
            )
            stax_review.path = str(child)

            # Set status if different
            if stax_review.status != orio_review.status.state:
                stax_review.status = orio_review.status.state

                # Set as updated
                stax_review.updated = True

            # Set Notes
            existing_notes = [stax_note.date for stax_note in stax_review.notes]
            for orio_note in orio_review.notes:
                if str(orio_note.date) not in existing_notes:
                    # Add note to stax_review, copy to cache eventually
                    stax_review.add_note(orio_note, self.copy_to_cache)

                    # Set as updated
                    stax_review.updated = True

                    # Something has been imported?
                    review_imported = True

        # Warn the user if nothing has been imported from the target dir is it's not default
        default_orio_dir = get_orio_default_cache_directory()
        if not review_imported and reviews_dir != default_orio_dir:
            self.report({"WARNING"}, f"No any review found in {reviews_dir}")

        # Link reviews to sequences and update statuses
        for sequence in bpy.data.scenes["Scene"].sequence_editor.sequences:
            media_sequence = get_media_sequence(sequence)
            stax_review = scene.reviews.get(str(get_source_media_path(media_sequence)))
            if stax_review:
                # Link review
                stax_review.sequence_name = media_sequence.name

                # Link sequence and propagate to meta hierarchy
                sequence_hierarchy = get_parent_metas(media_sequence) + [media_sequence]
                for seq in sequence_hierarchy:
                    set_sequence_attributes(seq, stax_review)

        # Show annotations
        bpy.ops.sequencer.show_sequences_annotations(override, "INVOKE_DEFAULT")

        # Highlight updated reviews
        bpy.ops.sequencer.highlight_updated_reviews({"scene": bpy.data.scenes["Scene"]})

        # Keep the source directory
        scene.reviews_linked_dir = self.directory

        # Update active parameters
        scene.frame_set(scene.frame_current)

        # Save session
        save_session()

        return {"FINISHED"}


class WM_OT_watch_review_start(bpy.types.Operator):
    """Automatically detect when a review starts"""

    bl_idname = "wm.watch_review_start"
    bl_label = "Auto detect review start"

    def execute(self, context):
        context.window_manager.modal_handler_add(self)
        return {"RUNNING_MODAL"}

    def modal(self, context, event):
        # Consider the review is started when the user clicks on the annotate tool
        if context.screen.get("current_tool", "").startswith("builtin.annotate"):
            # Automatically start review
            context.scene.review_session_active = True
            return {"FINISHED"}

        return {"PASS_THROUGH"}


class SCENE_OT_abort_reviews(bpy.types.Operator):
    """Clear the created reviews"""

    bl_idname = "scene.abort_reviews"
    bl_label = "Abort Reviews"

    @classmethod
    def poll(cls, context):
        if context.scene.review_session_active:
            return True

    def invoke(self, context, _event):
        # Ask to confirm
        return context.window_manager.invoke_confirm(self, _event)

    def execute(self, context):
        # Clear tmp cache dir
        tmp_dir = get_temp_directory()
        shutil.rmtree(tmp_dir, ignore_errors=True)

        # Copy the restore file as current blend session file and open it
        session_file = Path(bpy.data.filepath)
        if session_file.is_file():
            restore_file = Path(bpy.data.filepath).with_suffix(".restore")

            if restore_file.is_file():
                shutil.copy(restore_file, session_file)

                # Open blender restored file
                bpy.ops.wm.open_mainfile(filepath=session_file.as_posix())

                return {"FINISHED"}

        # Clear pending note contents for all sequences
        # -----
        for seq in bpy.context.scene.sequence_editor.sequences:
            media_sequence = get_media_sequence(seq)

            # Clear Advanced Drawings
            clear_advanced_drawings(media_sequence)

            # Annotate
            clear_annotate(media_sequence)

            # Text
            clear_text_content(media_sequence)

            # Set reviewed state
            media_sequence["reviewed"] = False
        # -----

        # Close review session
        context.scene.review_session_active = False

        # Watch any action considered to start the review session
        bpy.ops.wm.watch_review_start()

        # Set the picker as default tool back
        bpy.ops.wm.tool_set_by_id(get_context("Main", "PREVIEW"), name="builtin.sample")

        return {"FINISHED"}


class STAX_OT_publish_reviews(bpy.types.Operator):
    """Publish reviews of the current session"""

    bl_idname = "scene.publish_reviews"
    bl_label = "Publish Reviews"

    filter_glob: StringProperty(
        default="",
        options={"HIDDEN"},
    )

    filename_ext = ""

    directory: bpy.props.StringProperty(
        name="Outdir Path",
        description="Where I will save my stuff",
        subtype="DIR_PATH",
    )

    @classmethod
    def poll(cls, context):
        if context.scene.review_session_active:
            return True

    def __init__(self):
        self.directory = bpy.context.scene.reviews_linked_dir

    def invoke(self, context, event):
        # Tell to wait
        context.window.cursor_set("WAIT")

        # If Ctrl + shift + click ask to choose a folder with a filebrowser
        if event.ctrl and event.shift or not self.directory:
            context.window_manager.fileselect_add(self)
            return {"RUNNING_MODAL"}

        # Ask to confirm
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        scene = context.scene
        gp = bpy.data.grease_pencils["Annotations"]
        annotations_directory = Path(utils_cache.get_temp_directory(), "annotations")
        current_frame = scene.frame_current
        author = scene.user_preferences.user_login
        scene.reviews_linked_dir = self.directory

        # Set render settings
        scene.render.image_settings.file_format = "PNG"
        scene.render.image_settings.color_mode = "RGBA"
        scene.render.image_settings.compression = 70

        # Hide all strips to render only Annotations
        bpy.ops.sequencer.select_all(action="SELECT")
        bpy.ops.sequencer.unlock()
        bpy.ops.sequencer.mute()

        # Lock sequences back for disabled review, locked sequences aren't rendered
        bpy.ops.sequencer.lock_review()

        # OpenReviewIO building
        edited_notes = set()  # Notes and reviews to be passed to the callbacks
        annotation_notes = set()  # Store (stax_review, orio_note) for later add()

        # Text replies
        # ------------
        for text in bpy.data.texts:
            if text.name.startswith("REPLY_"):
                # Get review and parent note
                stax_review = scene.reviews.get(text.get("review_name"))
                orio_review = stax_review.as_orio_review()
                orio_parent_note = orio_review.get_note(text.get("note_name"))

                # Get text content
                content = utils_core.get_text_body(text)

                # Build comment
                text_comment = orio.Content.TextComment(body=content)

                # Create and add note to Stax
                note = orio.Note(
                    author=author,
                    contents=text_comment,
                    parent=orio_parent_note,
                )
                stax_review.add_note(note)

                # Keep note to pass it to callback
                edited_notes.add((orio_review, note))

        # Image annotations, text comments and statuses
        # ===================
        #  Hide all layers
        for layer in gp.layers:
            layer.hide = True

        #  Render for all frames with drawings
        stax_review = None
        orio_note = None
        annotated_sequences = []
        annotation_frames = set()  # To delete them in the end

        # To store {Frame number: (related sequence, ImageAnnotation)}
        # This keeps reference frames and avoid duplicates, to render only one reference by drawn frame
        reference_frames = {}

        visible_sequences = get_visible_sequences()

        for visible_sequence in visible_sequences:
            media_sequence = get_media_sequence(visible_sequence)

            if not media_sequence.get("reviewed"):
                continue

            # If sequence is locked, jump to next loop
            if visible_sequence.lock:
                continue

            # New/Get stax review and new note of current sequence
            # =======================================

            # Create default review if None already exists
            stax_review = get_collection_entity(
                scene.reviews,
                media_sequence.get(
                    "review_name", get_source_media_path(media_sequence).as_posix()
                ),
            )

            # Set media status to review
            stax_review.status = media_sequence.get("status", "waiting review")

            # Create note
            orio_note = orio.Note(author=author)

            # Dynamic function
            # -------------------
            def create_image_annotation(
                related_sequence, start_frame, duration, image_path
            ):
                """Create ORIO ImageAnnotation, add it to note and keep the reference for later rendering"""
                # Offset annotation start frame if video is offset, to be sure to be in media time
                annotation_frame = start_frame - related_sequence.frame_start
                content = orio.Content.ImageAnnotation(
                    frame=annotation_frame,
                    duration=duration,
                    path_to_image=image_path,
                )
                orio_note.add_content(content)

                # Reference Image
                if reference_frames.get((start_frame, related_sequence)):
                    reference_frames[(start_frame, related_sequence)].append(content)
                else:
                    reference_frames[(start_frame, related_sequence)] = [content]

                # Keep for cleaning
                annotated_sequences.append(media_sequence)

            # -------------

            # Annotations
            # ===========
            for layer in gp.layers:
                layer.hide = False  # Unhide layer to see it

                # Get frames in the sequence range if the frame is not already used as a reference
                all_frames = list(layer.frames)
                sequence_frames = []
                for i, frame in enumerate(all_frames):
                    if (
                        frame.frame_number
                        not in {f[0] for f in reference_frames.keys()}
                        and visible_sequence.frame_start
                        <= frame.frame_number
                        < visible_sequence.frame_final_end
                    ):
                        sequence_frames.append(frame)

                # Create related annotations
                for i, frame in enumerate(sequence_frames):
                    # Move playhead to frame
                    scene.frame_set(frame.frame_number)

                    # Render only if frame has drawings
                    if len(frame.strokes):
                        # Calculate duration. If last GP frame use end frame of sequence
                        if frame is sequence_frames[-1]:
                            annotation_duration = (
                                visible_sequence.frame_final_end - frame.frame_number
                            )
                        else:
                            annotation_duration = (
                                sequence_frames[i + 1].frame_number - frame.frame_number
                            )

                        # Build image name and path
                        annotation_name = f"annotation_{layer.info.replace('.', '-')}_{frame.frame_number}"
                        annotation_path = annotations_directory.joinpath(
                            annotation_name
                        ).with_suffix(
                            "." + scene.render.image_settings.file_format.lower()
                        )

                        # Set scene settings for render
                        scene.render.filepath = str(annotation_path)

                        # Export
                        # Override context to force render from the Preview
                        bpy.ops.render.opengl(
                            {"area": bpy.data.screens["Main"].areas[1]},
                            animation=False,
                            sequencer=True,
                            write_still=True,
                        )

                        create_image_annotation(
                            media_sequence,
                            frame.frame_number,
                            annotation_duration,
                            annotation_path,
                        )

                        # Keep for cleaning
                        annotation_frames.add((layer, frame))
            # ===============

            # #####
            # Add OpenGL rendered images
            # #####

            # Render for all rendered frames
            images_list = media_sequence.get("external_images", []) + (
                media_sequence.get("rendered_images", [])
            )
            for image in images_list:
                image_sequence = scene.sequence_editor.sequences_all.get(image)
                if image_sequence:
                    # Create ORIO content and add it to note
                    create_image_annotation(
                        media_sequence,
                        image_sequence.frame_start,
                        image_sequence.frame_final_duration,
                        Path(image_sequence.directory, image_sequence.name),
                    )

            # Create text contents
            # ====================
            comments, annotations = get_text_contents(
                media_sequence.get("text_contents", [])
            )

            # Create Text comment
            # -------------------
            text_comment = media_sequence.get("text_comment")
            for text_comment in comments:
                # Get text content
                body = utils_core.get_text_body(text_comment)

                # Add text comment to note
                content = orio.Content.TextComment(body=body)
                orio_note.add_content(content)

            # Create Text annotations
            # -----------------------
            for annotation in annotations:
                # Get text content
                body = utils_core.get_text_body(annotation)

                # Add text comment to note
                start_frame = annotation.get("frame_start")
                content = orio.Content.TextAnnotation(
                    body=body,
                    frame=start_frame,
                    duration=max(annotation.get("frame_end") - start_frame, 1),
                )
                orio_note.add_content(content)

            # Remove the text contents from the sequence
            media_sequence.pop("text_contents", None)

            if stax_review:
                # Add the note only if has contents, else the note is None
                # TODO not satisfying while it should keep only the review with changed status
                #   and the notes with contents, not all the reviews.
                #   New design has to be set but it's breaking change
                edited_notes.add(
                    (
                        stax_review.as_orio_review(),
                        orio_note if orio_note.contents else None,
                    )
                )
                annotation_notes.add((stax_review, orio_note))

        # Unhide all layers
        for layer in gp.layers:
            layer.hide = False

        #####################
        # Reference images
        #####################

        # Set render settings
        scene.render.image_settings.file_format = "JPEG"

        # Save screenshots of drawn frames and associate them as reference frame to contents
        images_paths = []
        sequences_to_clean = set()
        for reference, contents in reference_frames.items():
            frame_number, sequence = reference

            # Build name and path
            media_path = get_source_media_path(sequence)
            image_name = f"{media_path.stem}_retake_{frame_number}"
            image_path = utils_cache.get_temp_directory().joinpath(
                f"{image_name}.{scene.render.image_settings.file_format.lower()}"
            )

            # Set scene
            scene.frame_set(frame_number)
            scene.render.filepath = str(image_path)

            # Unmute sequence and all its parents
            all_parent_metas = get_parent_metas(sequence)
            for seq_meta in all_parent_metas:
                seq_meta.mute = False
            sequence.mute = False

            # Export
            images_paths.append(image_path)
            bpy.ops.render.opengl(animation=False, sequencer=True, write_still=True)

            # Mute sequence back and all its parents
            for seq_meta in all_parent_metas:
                seq_meta.mute = True
            sequence.mute = True

            # Add image as reference image to all ImageAnnotation contents
            for content in contents:
                content.reference_image = image_path

            # Keep this sequence to clean it
            sequences_to_clean.add(sequence)

        #####################
        # Publishing
        #####################

        # Export reviews
        # --------------
        target_dir = Path(self.directory).resolve()

        # Run publish callback
        if utils_linker.check_linker_func("review_session_publish"):
            utils_linker.get_linker().review_session_publish(edited_notes)
        else:  # Write reviews in folder
            if target_dir.is_dir():
                for orio_review, orio_note in edited_notes:
                    # Add note only if exists
                    if orio_note:
                        orio_review.add_note(orio_note)  # TODO not optimized

                    # Write review to target directory
                    orio_review.write(target_dir)

        # Add notes to reviews -> update reviews in Stax cache
        # After the callback to allow it to modify ORIO data before writing in cache
        for stax_review, orio_note in annotation_notes:
            stax_review.add_note(orio_note)

        #####################
        # Cleaning
        #####################

        # Delete opengl rendered images sequences
        # ---------------------------------------

        # For every sequence to clean
        # Delete Images from advanced drawing and Externally edited
        # Toggle metasequence and delete the "temp" metasequence
        # Delete also "externally_edited" sequences
        for sequence_to_clean in sequences_to_clean:
            # Unmute sequence
            sequence_to_clean.mute = False

            # Clean hierarchy
            all_metas = get_parent_metas(sequence_to_clean)

            # TODO will be removed with Blender > 2.92 by meta.sequences.remove() instead of recursive meta opening
            for meta in all_metas:
                scene.sequence_editor.active_strip = meta
                meta.select = True

                bpy.ops.sequencer.meta_toggle()  # Open meta
                for seq in meta.sequences:
                    if seq.get("kind", "") in {"temp", "externally_edited"}:
                        seq.select = True
                    else:
                        seq.select = False
                bpy.ops.sequencer.delete()

            # Close back meta hierarchy
            for meta in reversed(all_metas):
                # Unmute meta
                meta.mute = False

                # Set as not reviewed
                meta["reviewed"] = False

                # Close meta
                scene.sequence_editor.active_strip = None
                bpy.ops.sequencer.select_all(action="DESELECT")
                bpy.ops.sequencer.meta_toggle()

            # Unlink drawing: scene and gp
            drawing_scene = sequence_to_clean.get("drawing_scene", None)
            if drawing_scene:
                scene.sequence_editor.active_strip = sequence_to_clean
                bpy.ops.wm.properties_remove(
                    data_path="scene.sequence_editor.active_strip",
                    property="drawing_scene",
                )
                bpy.data.scenes.remove(drawing_scene)
                bpy.ops.wm.properties_remove(
                    data_path="scene.sequence_editor.active_strip", property="gp"
                )

        # Delete temp directory
        if annotations_directory.is_dir():
            shutil.rmtree(annotations_directory)

        # Delete annotations
        for annotation in annotation_frames:
            # Remove frame
            layer, frame = annotation
            layer.frames.remove(frame)

        # Delete text comments and replies
        for text in bpy.data.texts:
            bpy.data.texts.remove(text)

        # Set all sequences as not reviewed
        for seq in scene.sequence_editor.sequences_all:
            seq["reviewed"] = False

        # Unmute all strips
        bpy.ops.sequencer.select_all(action="SELECT")
        bpy.ops.sequencer.unlock()
        bpy.ops.sequencer.unmute()
        bpy.ops.sequencer.select_all(action="DESELECT")

        # Display annotations for all annotated sequences
        for visible_sequence in annotated_sequences:
            metas_history = get_parent_metas(visible_sequence)
            if metas_history:
                main_meta = metas_history[0]
                main_meta.select = True
            else:
                visible_sequence.select = True

        # Load published reviews
        bpy.ops.scene.link_reviews(directory=self.directory)
        bpy.ops.sequencer.show_sequences_annotations()

        # Lock review disabled sequences
        bpy.ops.sequencer.lock_review()

        # Set the picker as default tool back
        override = get_context("Main", "PREVIEW")
        bpy.ops.wm.tool_set_by_id(override, name="builtin.sample")

        # Close review session
        context.scene.review_session_active = False

        # Set back displayed frame before publishing
        scene.frame_set(current_frame)

        # Save session and keep this new state as default
        save_session()

        # Watch any action considered to start the review session
        bpy.ops.wm.watch_review_start()

        return {"FINISHED"}


class ReportMessage(bpy.types.Operator):
    """Report given message in the given type"""

    bl_idname = "wm.report_message"
    bl_label = "Report message"

    message: bpy.props.StringProperty(name="Report Message", default="No Message")
    type: bpy.props.StringProperty(name="Report Type", default="INFO")

    def execute(self, context):
        if context.area:
            # Trick to display the message for sure
            context.window_manager.modal_handler_add(self)
            return {"RUNNING_MODAL"}
        else:
            self.report({self.type}, self.message)
            return {"FINISHED"}

    def modal(self, context, event):
        self.report({self.type}, self.message)
        return {"FINISHED"}


classes = [
    SCENE_OT_abort_reviews,
    STAX_OT_publish_reviews,
    SCENE_OT_link_reviews,
    WM_OT_watch_review_start,
    ReportMessage,
    SCENE_OT_ChangeStaxConfiguration,
    WM_OT_ProductionManagerAuthentication,
    LoadPreviousSession,
]


def register():
    # Set callbacks # TODO refactor it when script reloading works
    available_callbacks = utils_linker.get_available_callbacks()

    if available_callbacks:
        user_config = utils_config.load_user_config()

        # If a callback file is directly set by the env var use it
        # Else test if the config matches a callback
        if os.getenv("STAX_LINKER_PATH"):
            callback_name = Path(os.getenv("STAX_LINKER_PATH")).stem
            callback_to_use = available_callbacks.get(callback_name)
        else:
            # If there's no "callback" attribute in the user configuration file, use no callback.
            user_config_callback = user_config.get("callback", "")
            callback_to_use = available_callbacks.get(user_config_callback)

        if callback_to_use:
            # Set by default the functions to None
            # TODO Not clean but needed for autodoc until https://gitlab.com/superprod/stax/-/issues/150
            Callbacks.authentication_exec = None
            Callbacks.session_update_pre = None
            Callbacks.review_session_publish = None

            bpy.utils.register_class(callback_to_use)
            bpy.types.WindowManager.stax_callback = bpy.props.PointerProperty(
                type=callback_to_use
            )

    # Custom Properties
    bpy.types.Scene.session_logged_in = bpy.props.BoolProperty(
        name="Logged in", default=False
    )
    bpy.types.Scene.session_logged_in = False

    bpy.types.Scene.is_timeline_loaded = bpy.props.BoolProperty(
        name="Is timeline loaded",
        default=False,
        description="Says if the timeline is correctly loaded.",
    )

    bpy.types.Scene.reviews_linked_dir = bpy.props.StringProperty(
        name="Reviews Linked Directory",
        description="Directory to link the reviews from. Load and publish reviews into this directory",
    )

    bpy.types.WindowManager.source_workspace = bpy.props.StringProperty(
        name="Source Workspace",
        default="Stax configuration",
        description="If called from other workspace, it keeps the source workspace to set it back at saving",
    )

    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    del bpy.types.WindowManager.stax_callback
    del bpy.types.WindowManager.source_workspace
    del bpy.types.Scene.is_timeline_loaded
    del bpy.types.Scene.session_logged_in

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
