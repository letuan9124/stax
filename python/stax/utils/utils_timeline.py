# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to timeline management
"""
from collections.abc import Iterable
from pathlib import Path
import textwrap
from typing import List, MethodDescriptorType, Tuple, Union

import bpy
from bpy.app.handlers import persistent
from bpy.types import ImageSequence, MetaSequence, MovieSequence, Sequence, WipeSequence
from idprop.types import IDPropertyGroup
import opentimelineio as otio

import stax
from .utils_cache import (
    copy_media_to_cache,
    clean_outdated_cache,
    delete_session_cache,
    get_all_sessions_cache_directory,
    get_session_cache_file,
    read_cache_timeline,
    start_from_default_file,
    write_cache_timeline,
)
from .utils_core import autosave, get_context, get_selected_sequences, wrap_text
from .utils_linker import get_linker

# Dict for dynamic media info panels
info_panels = {}


def get_current_sequence():
    """Get the current displayed sequence

    TODO: Will be optimized with: https://developer.blender.org/D7679
    """
    scene = bpy.data.scenes["Scene"]

    screen = bpy.data.screens["Main"]

    # Keep selected sequences
    kept_selected_sequences = get_selected_sequences()

    # Select under playhead, optimize it with https://developer.blender.org/D7679 when it lands
    for seq in bpy.data.scenes["Scene"].sequence_editor.sequences:
        if (
            seq.frame_start <= scene.frame_current
            and seq.frame_final_end > scene.frame_current
        ):
            seq.select = True
        else:
            seq.select = False

    # Selected sequences are sorted by Blender from the lowest to the highest
    selected_sequences = get_selected_sequences()

    # Return the clip matching the displayed channel or the visible one
    # --------

    # Get the main preview
    sq_preview_areas = [
        x
        for x in screen.areas
        if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
    ]
    displayed_track_channel = sq_preview_areas[-1].spaces[0].display_channel

    # Get the current displayed sequence
    current_sequence = None
    for i, seq in enumerate(selected_sequences):
        if seq.channel == displayed_track_channel:
            current_sequence = seq
            break
        elif seq.channel > displayed_track_channel:
            current_sequence = selected_sequences[i - 1]
            break
        else:
            current_sequence = selected_sequences[-1]

    # Set back selected sequences
    bpy.ops.sequencer.select_all({"scene": bpy.data.scenes["Scene"]}, action="DESELECT")
    for seq in kept_selected_sequences:
        seq.select = True

    return current_sequence


def get_media_sequence(input_sequence: Sequence) -> Union[MovieSequence, ImageSequence]:
    """Return the media_sequence for a given input_sequence.

    The media sequence is the sequence of the currently displayed media, and the highest one in the tracks when blending.
    It can only be an image or a movie.
    Useful when the input_sequence is a MetaSequence, return the main displayed media sequence by iterating in sub-sequences.
    If input_sequence has `media_sequence_name` set, try to return the sequence with this name.

    :param input_sequence: Sequence to get the media sequence from
    :return: media sequence (image or movie media sequence)
    """
    scene = bpy.context.scene

    # Sentinel
    if not input_sequence:
        return

    # Get media sequence
    sequence_editor = bpy.data.scenes["Scene"].sequence_editor
    all_sequences = sequence_editor.sequences_all
    media_sequence_name = input_sequence.get("media_sequence_name", "")

    # Fall back on Movie or Image sequence
    if not media_sequence_name:
        # Return current sequence if not a meta and the playhead is over it
        if (
            input_sequence.type in ["IMAGE", "MOVIE", "SOUND"]
            and input_sequence.frame_final_start
            <= scene.frame_current
            < input_sequence.frame_final_end
        ):
            return input_sequence
        # If meta, iterate in hierarchy to find the current one from the highest seq to the bottom
        elif input_sequence.type == "META":
            for seq in reversed(input_sequence.sequences):
                return get_media_sequence(seq)

    return all_sequences.get(media_sequence_name) or input_sequence


def get_visible_sequences() -> List[MovieSequence]:
    """Return a list of all the visible sequences.

    .. code-block::

        [=]: selected sequence, [x]: not selected sequence, ->: active track
        -------------------------------
        |     [xxxxxxx]                |
        |->  [===][=][===]               |
        |   [====][xxx]  [====]        |
        |  [====][xxxxx]        [====] |
        --------------------------------

    :return: List of visible sequences"""
    scene = bpy.context.scene

    # Ensure current sequence
    scene.frame_set(scene.frame_current)
    current_sequence = scene.sequence_editor.active_strip

    # Sentinel
    if not current_sequence:
        return set()

    # Try cached data
    cached_visible_sequences = scene.get("visible_sequences")
    if cached_visible_sequences:
        return frozenset(
            {
                scene.sequence_editor.sequences.get(name)
                for name in cached_visible_sequences
            }
        )

    # Iterate all sequences
    visible_sequences = set()
    filled_ranges = set()  # Keep filled ranges for optim
    active_channel = current_sequence.channel
    for sequence in reversed(scene.sequence_editor.sequences):
        # Match all sequences with current channel or below
        if sequence.channel == active_channel:
            visible_sequences.add(sequence)
            filled_ranges.add((sequence.frame_final_start, sequence.frame_final_end))
        elif sequence.channel < active_channel:
            # Get the current displayed sequence
            for frame in range(scene.frame_start, scene.frame_end):
                # Check if range is already filled by a sequence
                # ---------
                range_filled = False
                for filled_range in filled_ranges:
                    if filled_range[0] <= frame < filled_range[1]:
                        range_filled = True
                        break

                if range_filled:
                    continue
                # ---------

                # Check if frame is between sequence's boundaries
                if sequence.frame_final_start <= frame < sequence.frame_final_end:
                    visible_sequences.add(sequence)
                    filled_ranges.add(
                        (sequence.frame_final_start, sequence.frame_final_end)
                    )

                    # Continue to next sequence
                    continue

    # Set active sequence back
    if current_sequence:
        bpy.ops.sequencer.select_all(action="DESELECT")
        current_sequence.select = True
        scene.sequence_editor.active_strip = current_sequence

    return frozenset(visible_sequences)


def select_overlapping_sequences(sequence: MovieSequence):
    """Select overlapping sequences of given sequence

    :param sequence: Reference sequence
    """
    bpy.context.scene.sequence_editor.active_strip = sequence
    bpy.ops.sequencer.select_grouped(type="OVERLAP")
    return get_selected_sequences()


def select_above_sequences(sequence: MovieSequence):
    """Select above sequences of given sequence

    :param sequence: Reference sequence
    """
    above_sequences = []
    for seq in select_overlapping_sequences(sequence):
        if seq.channel > sequence.channel:
            above_sequences.append(seq)
        else:
            # If sequence below or equal deselect
            seq.select = False
    return above_sequences


def get_sequence_in_list(
    name: str, sequences_list: List[bpy.types.Sequence]
) -> bpy.types.Sequence:
    """Get sequence by its name in list, even if annotated

    :param name: Name of sequence to get
    :return: Catched sequence
    """
    # Get sequence of version
    sequence = sequences_list.get(name)
    # If doesn't exist, try with meta name
    if not sequence:
        sequence = sequences_list.get(f"{name}.")
        # If doesn't exist, try with annotated
        if not sequence:
            sequence = sequences_list.get(f"{name}.annotated")

    return sequence


def get_source_media_path(sequence: Sequence) -> Path:
    """Return source media path of a given sequence.

    :param sequence: Sequence to get source media path
    :return: Path of source media
    """
    if sequence.type == "IMAGE":
        return Path(sequence.directory, sequence.elements[0].filename)
    elif sequence.type == "SOUND":
        return Path(sequence.sound.filepath)
    else:
        return Path(sequence.filepath)


def get_parent_metas(sequence: Sequence) -> List[MetaSequence]:
    """Return all meta sequences hierarchy of the given sequence.

    :param sequence: Sequence to get meta sequences hierarchy
    :return: List of all meta sequences from the main view to the most nested one
    """
    sequence_editor = bpy.context.scene.sequence_editor
    last_meta = None
    all_metas = []

    # Sentinel for sequence into main timeline
    if sequence_editor.sequences.get(sequence.name):
        return all_metas

    # Match sequence
    for seq in sequence_editor.sequences_all:
        if seq == sequence:
            return all_metas
        else:
            if seq.type == "META":
                if sequence_editor.sequences.get(seq.name):
                    all_metas = [seq]
                else:
                    all_metas.append(seq)

                last_meta = seq


def get_wipe_sequence(
    sequence: Union[MovieSequence, MetaSequence, ImageSequence]
) -> WipeSequence:
    """Get wipe sequence of given sequence, create one if doesn't exist.

    :param sequence: Sequence to get wipe sequence from
    :return: Wipe sequence of given sequence
    """
    if not sequence.get("wipe"):
        wipe_sequence, _ = add_wipe_effect(sequence)
    else:
        wipe_sequence = bpy.context.scene.sequence_editor.sequences_all.get(
            sequence["wipe"]
        )

    return wipe_sequence


def load_timeline(timeline_path: Union[Path, str]):
    """Load timeline from the given path into Stax internal data structure.

    :param timeline_path: Timeline file to load
    """
    scene = bpy.context.scene
    timeline_path = Path(timeline_path)

    # Open previous session if exists, else create new
    session_file = get_session_cache_file(timeline_path)

    #   Open .blend file
    if session_file.is_file():
        # Open blender cache file
        bpy.ops.wm.open_mainfile(filepath=str(session_file))

    else:
        # Start from default file
        start_from_default_file(session_file)

    # Update Scene
    scene = bpy.context.scene

    # Clean outdated cache
    clean_outdated_cache(
        expiration_delay=scene.user_preferences.cache_expiration_delay,
        ignore_timelines=[timeline_path],
    )

    # Load version
    #   Reset cache if current Stax version is different
    if scene.stax_info.version != stax.version:
        delete_session_cache(timeline_path)

        # Start from default file
        start_from_default_file(session_file)

        # Update Scene
        scene = bpy.context.scene

        setattr(scene.stax_info, "version", stax.version)

    # Set timeline link
    scene.timeline = timeline_path.as_posix()

    # Run callback
    linker = get_linker()
    if linker and getattr(linker, "session_update_pre", None):
        linker.session_update_pre(scene.timeline, get_all_sessions_cache_directory())

    # Compare current timeline cache data and source timeline
    # -------------------------------------------------------
    # Read cache timeline
    cache_timeline = read_cache_timeline(timeline_path)

    # Read source timeline
    source_timeline = otio.adapters.read_from_file(timeline_path.as_posix())

    # Compare and don't update if editing haven't changed
    if cache_timeline and source_timeline.is_equivalent_to(cache_timeline):
        bpy.ops.wm.report_message(type="INFO", message="Timeline already up to date")
    else:
        # Clear tracks data to avoid wrong update
        scene.tracks.clear()

        # Re-Build timeline
        update_timeline(scene, source_timeline)

    # Lock review disabled sequences
    bpy.ops.sequencer.lock_review()

    # Add timer for autosave
    autosave_delay = scene.user_preferences.autosave_delay * 60

    bpy.app.timers.register(autosave, first_interval=autosave_delay, persistent=True)

    # Update active parameters
    scene.frame_set(scene.frame_current)


def update_timeline(scene, source_timeline: otio.schema.Timeline):
    """Update the timeline's editing

    :param scene: Blender scene to build the editing in
    :param source_timeline: Timeline to build the editing from
    """
    user_preferences = scene.user_preferences

    # Set frame rate
    preset_script_path = Path(
        f"{bpy.utils.preset_paths('framerate')[0]}/{source_timeline.metadata.get('frame_rate', '25')}.py"
    )
    if preset_script_path.is_file():
        exec(compile(preset_script_path.open().read(), preset_script_path, "exec"))
    else:
        bpy.ops.wm.report_message(
            type={"WARNING"},
            message=f"The preset: '{source_timeline.metadata.get('frame_rate')} fps' doesn't exist. Default framerate kept: {scene.render.fps/scene.render.fps_base}.",
        )

    # Get all cut keyframes, frames of GP annotation with empty keyframe
    cut_keyframes = {
        f.frame_number
        for f in bpy.data.grease_pencils["Annotations"].layers["Note"].frames.values()
    }

    # Clean timeline TODO needs to be optimized
    bpy.ops.sequencer.select_all(action="SELECT")
    bpy.ops.sequencer.delete()

    # Create markers
    if not len(scene.timeline_markers):
        scene.timeline_markers.clear()

        # Create markers
        # TODO: Import markers from the OTIO Timeline
        # https://opentimelineio.readthedocs.io/en/latest/tutorials/time-ranges.html#markers
        # https://gitlab.com/superprod/stax/issues/26
        reference_track = source_timeline.tracks[-1]
        for i, cut in enumerate(
            reference_track.each_child(descended_from_type=otio.schema.Stack), 1
        ):
            if cut.parent() is reference_track:
                scene.timeline_markers.new(
                    str(i), frame=cut.range_in_parent().start_time.value
                )

        # Set timeline boundaries
        scene.frame_start = 0
        scene.frame_end = scene.frame_start + source_timeline.duration().value
        #   Create empty GP Annotations frame at the end
        if scene.frame_end not in cut_keyframes:
            bpy.data.grease_pencils["Annotations"].layers["Note"].frames.new(
                scene.frame_end
            )

    # Set tracks and elements
    for track in source_timeline.tracks:

        current_user_track = scene.tracks.get(track.name)
        if not current_user_track:
            current_user_track = scene.tracks.add()
            setattr(current_user_track, "name", track.name)
        setattr(current_user_track, "displayed", True)

        # Create track elements
        if current_user_track:
            for stack in track.each_child(descended_from_type=otio.schema.Stack):
                current_frame_number = stack.range_in_parent().start_time.value

                # Add GP annotation empty keyframe for every cut
                if current_frame_number not in cut_keyframes:
                    # TODO report frames.get()/keys() doesn't work as expected for GP
                    bpy.data.grease_pencils["Annotations"].layers["Note"].frames.new(
                        current_frame_number
                    )
                    cut_keyframes.add(current_frame_number)

                if stack.parent() is track:
                    current_element = current_user_track.elements.get(stack.name)
                    if not current_element:
                        current_element = current_user_track.elements.add()
                        setattr(current_element, "name", stack.name)

                    for key, value in stack.metadata.items():
                        setattr(current_element, key, value)

                    # Create versions
                    for child in stack.each_child(
                        descended_from_type=otio.schema.Track
                    ):
                        if child.parent() is stack:
                            current_version = current_element.versions.get(child.name)
                            if not current_version:
                                current_version = current_element.versions.add()
                                setattr(current_version, "name", child.name)

                            setattr(
                                current_version,
                                "frame_start",
                                current_frame_number,
                            )
                            setattr(
                                current_version,
                                "clip_offset_start",
                                child.source_range.start_time.value
                                if child.source_range
                                else 0,
                            )
                            setattr(
                                current_version,
                                "duration",
                                child.source_range.duration.value
                                if child.source_range
                                else stack.range_in_parent().duration.value,
                            )

                            # Set version attributes
                            # TODO add a warning if metadata is set on the Track, in order to switch to VSE IO importer
                            for version_k, version_v in child.metadata.items():
                                current_version[version_k] = version_v

                            # Set movie clip paths
                            clips_list = list(child.each_clip())
                            if clips_list:
                                # Convert the media path to absolute path
                                absolute_media_path = Path(
                                    scene.timeline,
                                    clips_list[0].media_reference.target_url,
                                ).resolve()

                                # Set media path to version
                                setattr(
                                    current_version,
                                    "filepath",
                                    str(absolute_media_path),
                                )

                                # Cache media if required
                                if user_preferences.cache_media:
                                    setattr(
                                        current_version,
                                        "cache_filepath",
                                        str(
                                            copy_media_to_cache(
                                                absolute_media_path,
                                            )
                                        ),
                                    )

                                # Also add metadata from clip
                                for clip_k, clip_v in clips_list[0].metadata.items():
                                    current_version[clip_k] = clip_v

                            else:
                                bpy.ops.wm.report_message(
                                    type="WARNING",
                                    message="No clip for: " + child.name,
                                )

        scene.current_displayed_track = current_user_track.name

    # Update displayed tracks
    update_tracks()
    update_preview_channel()

    # Write OTIO timeline in file
    write_cache_timeline(scene.timeline, source_timeline)

    scene.is_timeline_loaded = True

    # Resize timeline to selected sequences (based on highlight)
    override = get_context("Main", "SEQUENCER")
    bpy.ops.sequencer.select_all(action="SELECT")
    bpy.ops.sequencer.view_selected(override)


def update_preview_channel():
    """Update track displayed in the preview by changing the channel."""
    context = bpy.context
    scene = context.scene
    screen = context.screen

    if screen:
        #   Set the correct display_channel to the space data.
        # If it is run from the preview space, then update the channel (choose track to display enum list)
        # Else, iterate in every area of the screen and when first PREVIEW is encountered, change its channel
        if context.space_data:
            current_space = None
            if (
                hasattr(context.space_data, "view_type")
                and context.space_data.view_type == "PREVIEW"
            ):
                current_space = context.space_data
            else:
                for area in reversed(screen.areas):
                    space = area.spaces[0]
                    if hasattr(space, "view_type") and space.view_type == "PREVIEW":
                        current_space = space
                        break
                    else:
                        current_space = context.space_data
        else:
            current_space = get_context("Main", "PREVIEW").get("space_data")

        current_space.display_channel = (
            scene.tracks.find(scene.current_displayed_track) + 1
        )

        bpy.ops.wm.report_message(
            type="INFO", message=f"Track displayed: {scene.current_displayed_track}"
        )

        # Update the active strip
        scene.frame_set(scene.frame_current)

    # Update comparison
    compare_properties = scene.compare_properties
    compare_properties.compare_type = compare_properties.compare_type


def update_tracks():
    """Update displayed tracks in Stax timeline.

    :param already_displayed_tracks: Tracks already displayed. They'll be ignored at updating.
    """
    context = bpy.context
    scene = context.scene
    override = get_context("Main", "SEQUENCER")

    #   Update displayed tracks on timeline
    # Move all created sequences out of scope
    bpy.ops.sequencer.select_all(override, action="SELECT")

    # Unlock
    bpy.ops.sequencer.unlock(override)
    # Move
    if hasattr(context, "selected_sequences"):
        bpy.ops.transform.seq_slide(override, value=(scene.frame_end, 0))
    bpy.ops.sequencer.select_all(override, action="DESELECT")

    # Update tracks
    for track in scene.tracks:
        track.update()

    # Sentinel to give message if there is no sequence
    if not scene.sequence_editor.sequences:
        bpy.ops.wm.report_message(
            type="ERROR",
            message="No media sequence have been created. Please make sure the media are accessible.",
        )

    # Lock review disabled sequences
    bpy.ops.sequencer.lock_review()

    # Update viewer
    update_preview_channel()

    # Write savings
    bpy.ops.wm.save_mainfile()


def display_media_annotations(media_sequence: Sequence, display_state: bool) -> bool:
    """Set the display state of media annotations

    :param sequence: Media sequence with annotations
    :param display: Display state to set to the annotations
    :return: Boolean if any annotations have been displayed
    """
    any_annotation_displayed = False

    metas_hierarchy = get_parent_metas(media_sequence)
    if metas_hierarchy:
        # Get direct parent meta sequence where annotations sequences are stored
        parent_meta = metas_hierarchy[-1]
        for seq in parent_meta.sequences:
            if seq.get("kind") in {
                "note",
                "annotation",
                "temp",
                "externally_edited",
            }:
                seq.mute = not display_state
                any_annotation_displayed = True

    return any_annotation_displayed


def make_meta(sequences: List[MovieSequence]) -> MetaSequence:
    """Convert the given sequence(s) to a meta sequence and associate the data.

    By default, the last sequence is used as main sequence.

    :param sequence: Sequence to convert to a meta
    :return: The created meta sequence
    """
    sequence_editor = bpy.context.scene.sequence_editor
    raw_types = {"IMAGE", "MOVIE", "SOUND"}

    # Select only the target sequence
    bpy.ops.sequencer.select_all(action="DESELECT")
    main_sequence = sequences[-1]  # Default the last given
    bpy.context.scene.sequence_editor.active_strip = main_sequence

    # Select sequences
    for seq in sequences:
        # Select sequence
        seq.select = True

    # Create meta
    bpy.ops.sequencer.meta_make()

    # Set source sequence attributes to created metastrip
    meta = sequence_editor.active_strip
    meta.name = f"{main_sequence.name}."
    meta.frame_final_duration = main_sequence.frame_final_duration
    meta.channel = main_sequence.channel
    for key, value in main_sequence.items():
        meta[key] = value

    # Set blend type by default
    meta.blend_type = "CROSS"

    # Keep the source sequence as reference for fast access from sequence_editor.sequences_all
    if main_sequence.type in raw_types:
        meta["media_sequence_name"] = (
            sequences[-1].get("media_sequence_name") or main_sequence.name
        )

    return meta


def add_overlay_to_sequence(
    sequence: Union[MetaSequence, MovieSequence],
    create_overlay: Tuple[MethodDescriptorType, dict],
) -> MetaSequence:
    """Add an overlay to a sequence.

    :param sequence: Sequence to add the overlay on
    :param create_overlay: Method to create a sequence added as overlay, must return the created sequence
    :return: Return the created/modified meta sequence
    """
    context = bpy.context
    scene = context.scene
    sequence_editor = scene.sequence_editor
    # Sentinel if the sequence already has an overlay
    # sequence = get_sequence_in_list(sequence.name, sequence_editor.sequences)
    all_metas = []
    sequence_editor.active_strip = sequence

    # Create meta if current sequence is in main timeline and not nested
    if sequence_editor.sequences.get(sequence.name):
        parent_meta = make_meta([sequence])
        all_metas = [parent_meta]
    else:
        all_metas = get_parent_metas(sequence)
        parent_meta = all_metas[-1]

    # Open meta hierarchy
    # TODO gonna be removed with 2.92
    for meta in all_metas:
        meta.select = True
        sequence_editor.active_strip = meta

        bpy.ops.sequencer.meta_toggle()  # Open meta

    # Create overlay
    if type(create_overlay) is tuple:
        func, kwargs = create_overlay
        overlay = func(parent_meta, **kwargs)
    else:
        overlay = create_overlay(parent_meta)

    if overlay and not parent_meta.name.endswith("annotated"):
        parent_meta.name = f"{parent_meta.name}annotated"

    # Deselect all metasequences
    if type(sequence) is MetaSequence:
        for seq in sequence.sequences:
            seq.select = False

    # Close meta hierarchy
    # TODO gonna be removed with 2.92
    for meta in all_metas:
        # Deselect to avoid entering in a newly created meta
        bpy.ops.sequencer.select_all(action="DESELECT")

        sequence_editor.active_strip = None

        bpy.ops.sequencer.meta_toggle()  # Close meta

    return overlay


def set_sequence_attributes(sequence: MetaSequence, stax_review):
    """Set attributes to a sequence from the related review for fast operations in sequencer.

    Also updates the raw sequence.

    :param sequence: Sequence to set attributes on
    :param stax_review: Review to get the attributes from
    """
    sequence["status"] = stax_review.status
    sequence["updated"] = stax_review.updated

    # Link sequence
    sequence["review_name"] = stax_review.name

    # Update raw sequence
    media_sequence = get_media_sequence(sequence)
    if media_sequence != sequence:
        set_sequence_attributes(media_sequence, stax_review)


def set_preview_range_from_sequences(
    sequences: List[Union[MetaSequence, MovieSequence]]
):
    """Set timeline preview range to sequences boundaries

    :param sequences: Sequences to set the preview range from
    """
    scene = bpy.context.scene

    # Get boundaries
    start = scene.frame_end
    end = scene.frame_start
    for seq in sequences:
        start = min(start, seq.frame_start)
        end = max(end, seq.frame_final_end) - 1

    scene.use_preview_range = True
    scene.frame_preview_start, scene.frame_preview_end = (
        start,
        end,
    )


def emphasize_drawings(holder, context):
    """Emphasize drawings by changing the color balance of visible sequences.

    As the visible sequences are metasequences, it's required to get the movie sequences inside them, else the adv. drawings are modified too.

    :param holder: Current Blender property's holder
    :param context: Current context
    """
    sequence_editor = bpy.data.scenes["Scene"].sequence_editor
    current_sequence = sequence_editor.active_strip

    # Sentinel if Main scene, it makes blender crash...  TODO: Check regularily if fixed, hard to reproduce and make an issue
    if context.scene is bpy.data.scenes["Scene"]:
        for seq in sequence_editor.sequences:
            media_sequence = get_media_sequence(seq)

            # Sentinel for non graphic sequences
            if media_sequence.type not in {"IMAGE", "MOVIE"}:
                continue

            # Create color balance modifier if doesn't exist
            modifier = media_sequence.modifiers.get("emphasize")
            if not modifier:
                color_balance = media_sequence.modifiers.new(
                    "emphasize", "COLOR_BALANCE"
                ).color_balance
            else:
                color_balance = modifier.color_balance

            # Change color balance value to make the sequences whiter
            value = ((holder.emphasize_drawings - 0) / (100 - 0)) * (2.0 - 1.0) + 1.0
            color_balance.lift = (value, value, value)

        # Force Update sequencer TODO: required while refresh of metastripped sequences is broken
        bpy.ops.sequencer.refresh_all()

    # Set current sequence as active back
    sequence_editor.active_strip = current_sequence

    # Update Adv. Drawing background image display and alpha
    bg_empty = bpy.data.objects["Background"]
    bg_empty.use_empty_image_alpha = True
    bg_empty.color[3] = (100 - holder.emphasize_drawings) / 100


def set_reviewed_state(sequence: Sequence):
    """Set "reviewed" state to the sequence.

    False by default but checks all possible review modifications.

    :param sequence: Sequence to be checked
    """
    scene = bpy.context.scene
    reviewed = False

    # External, Adv. Drawing and text comment
    if (
        sequence.get("external_images")
        or sequence.get("rendered_images")
        or sequence.get("text_contents")
    ):
        reviewed = True

    # Status changed
    if not reviewed:
        stax_review = scene.reviews.get(sequence.get("review_name", ""))

        if (stax_review and stax_review.status != sequence.get("status")) or (
            not stax_review and sequence.get("status")
        ):
            reviewed = True

    # Annotate tool
    if not reviewed:
        gp = bpy.data.grease_pencils["Annotations"]

        # Check if annotate frames with strokes are over the sequence
        for layer in gp.layers:
            all_frames = list(layer.frames)
            sequence_frames = []
            for i, frame in enumerate(all_frames):
                if (
                    sequence.frame_start
                    <= frame.frame_number
                    < sequence.frame_final_end
                    and len(frame.strokes)
                ):
                    reviewed = True

                    break

    # Set reviewed state to sequence
    sequence["reviewed"] = reviewed


def compare_refresh(_h, _s):
    """Refresh compare.

    Hack until Blender VSE refresh is optimized.
    """

    # Refresh the sequencer TODO: probably won't be needed after this fix: https://developer.blender.org/T79922
    bpy.ops.sequencer.refresh_all()


def add_wipe_effect(
    sequence: Union[MovieSequence, MetaSequence, ImageSequence]
) -> Tuple[WipeSequence, MetaSequence]:
    """Add wipe effect to the given sequence.

    Create an empty text sequence and a wipe, and stack them into a meta sequence.
    Add drivers to wipe parameters.

    TODO Will be optimized with 2.92 https://gitlab.com/superprod/stax/-/issues/300

    :param sequence: Sequence to add wipe to
    :return: Created wipe sequence, meta sequence containing wipe sequences structure
    """
    scene = bpy.context.scene
    sequence_editor = scene.sequence_editor
    wipe_sequence = None

    # If not meta sequence create one
    metas_hierarchy = get_parent_metas(sequence) or [make_meta([sequence])]
    parent_meta = metas_hierarchy[-1]

    # Open meta hierarchy
    for meta in metas_hierarchy:
        bpy.ops.sequencer.select_all(action="DESELECT")
        sequence_editor.active_strip = meta
        meta.select = True
        bpy.ops.sequencer.meta_toggle()  # Open Meta

    bpy.ops.sequencer.select_all(action="DESELECT")

    # Set as active
    sequence.select = True
    sequence_editor.active_strip = sequence

    # Substitute transform sequence to sequence if there is one
    for seq in parent_meta.sequences:
        if seq.type == "TRANSFORM" and seq.input_1 == sequence:
            sequence = seq
            break

    # Create empty text for transparency
    # --------------
    text_sequence = sequence_editor.sequences.new_effect(
        name=f"{sequence.name}.text_wipe_holder",
        type="TEXT",
        channel=sequence.channel + 1,
        frame_start=sequence.frame_start,
        frame_end=sequence.frame_final_end,
    )
    text_sequence.text = ""
    text_sequence.blend_type = "ALPHA_OVER"

    # Create wipe effect
    # ------------------
    text_sequence.select = True
    sequence.select = True
    wipe_sequence = sequence_editor.sequences.new_effect(
        name=f"{sequence.name}.wipe_effect",
        type="WIPE",
        channel=sequence.channel + 2,
        frame_start=sequence.frame_start,
        seq1=text_sequence,
        seq2=sequence,
    )

    # Disable default fade to be able to control the wipe
    wipe_sequence.use_default_fade = False
    wipe_sequence.effect_fader = 1

    # Add driver for wipe
    # ---------------------

    # NB: Wipe fader driver is not added there because not always active

    # Wipe angle
    add_single_driver(
        wipe_sequence,
        "angle",
        "SCENE",
        scene,
        "compare_properties.wipe_angle",
    )

    # Wipe blur
    add_single_driver(
        wipe_sequence,
        "blur_width",
        "SCENE",
        scene,
        "compare_properties.wipe_blur",
    )
    # ---------------------

    # Add wipe reference for fast catch
    sequence["wipe"] = wipe_sequence.name

    # Close meta hierarchy
    for meta in reversed(metas_hierarchy):
        sequence_editor.active_strip = None
        bpy.ops.sequencer.select_all(action="DESELECT")
        bpy.ops.sequencer.meta_toggle()  # Close Meta

    return wipe_sequence, parent_meta


def add_single_driver(target, data_name: str, id_type: str, id_obj, data_path: str):
    # Add driver
    fader_driver = target.driver_add(data_name).driver
    fader_driver.type = "AVERAGE"

    # Create variable
    var_name = data_path.replace(".", "__")
    fader_var = fader_driver.variables.get(var_name)
    if not fader_var:
        fader_var = fader_driver.variables.new()
        fader_var.name = var_name
    fader_var.type = "SINGLE_PROP"

    # Set variable parameters
    fader_var.targets[0].id_type = id_type
    fader_var.targets[0].id = id_obj
    fader_var.targets[0].data_path = data_path


def append_to_object_key(bl_object, item_key: str, item):
    """Append item (blender object) to a blender object under a key.

    Equivalent to bl_object[item_key].append(item)

    :param bl_object: Blender object to append the element to
    :param item_key: Key to append the element to
    :param item: Blender object to append
    """
    # Associate to sequence
    if not bl_object.get(item_key):
        # NB: Python's "set" is not a possible type, forced to use a list
        bl_object[item_key] = []

    # NB: Cannot use list.append()
    existing_objects = list(bl_object.get(item_key))
    bl_object[item_key] = [o for o in existing_objects if o] + [item]


# ======== Display info ===========
# TODO Should be in utils_ui but cannot because of circular dependencies.


def display_items_info(context, layout: bpy.types.UILayout, info: IDPropertyGroup):
    """Display information tree into layout.

    The display is different if this is a dict or a list like object.
    All iterables but string are formatted the same way and the "key" helps to determine the display case.

    :param layout: Layout to draw the infos into
    :param info: Information to display
    """
    # Conform formatting
    if isinstance(info, Iterable) and type(info) not in [str, IDPropertyGroup]:
        items = [(None, v) for v in info]
    elif isinstance(info, IDPropertyGroup):
        items = info.items()
    else:
        print(f"Unknown item type {info}: {type(info)}")

    for key, value in items:
        # Iterate nested values or display them
        if type(value) is IDPropertyGroup:  # Iterate
            box = layout.box()

            # Different display if it's a list or a dict
            if key:
                box.label(text=key, icon="RIGHTARROW")
                lyt = box
            else:
                lyt = box.column(align=True)

            display_items_info(context, lyt, value)

        elif isinstance(value, Iterable) and type(value) not in [str]:  # Display list
            # Box for list
            box = layout.box()
            if key:
                box.label(text=key)

            # Column split
            col = box.column(align=True)
            split = col.split(factor=0.01)
            split.column()
            right = split.column(align=True)

            # Display elements
            display_items_info(context, right, value)
        else:  # Display unique value

            # Red Color
            if type(value) in [bool, int] and (not value or value < 0):
                row = layout.row()
                row.alert = True

            # Display wrapped text
            # TODO look to mutualize it with other occurrences
            value = str(value)
            tool_shelf = None
            area = context.area

            for region in area.regions:
                if region.type == "UI":
                    tool_shelf = region

            width = tool_shelf.width / 8

            wrapper = textwrap.TextWrapper(
                width=width, break_long_words=False, replace_whitespace=False
            )
            wrapped_lines = wrap_text(wrapper, value)

            col = layout.column(align=True)
            if len(wrapped_lines) > 1:
                if key:
                    col.label(text=f"{key}:")
                for line in wrapped_lines:
                    row = col.row()
                    row.label(text=line)
            else:
                if key:
                    col.label(text=f"{key}: {value}")
                else:
                    col.label(text=value)


def dyn_panel_draw(self, context):
    """Draw function for info display dynamic panels."""
    layout = self.layout

    display_items_info(context, layout, self.informations)


class PREVIEW_PT_category_info(bpy.types.Panel):
    """All current media info sub panels"""

    bl_label = "Media Info"
    bl_region_type = "UI"
    bl_category = "Info"
    bl_space_type = "SEQUENCE_EDITOR"
    bl_parent_id = "PREVIEW_PT_media_info"


def update_media_info_panels(sequence: Sequence):
    """Create panels for media info depending on the "stax_media_info" property contents of the sequence.

    :param sequence: Sequence handling the media info
    """
    info_to_display = sequence.get("stax_media_info")

    if info_to_display:  # Create panels for info data
        try:
            for key, value in info_to_display.items():
                # Dynamically create panel class
                panel = type(
                    f"PREVIEW_PT_{key}",
                    (PREVIEW_PT_category_info,),
                    {
                        "bl_label": key,
                        "bl_description": f"{key} informations",
                        "informations": value,
                        "draw": dyn_panel_draw,
                    },
                )

                # Unregister panel and remove it
                if "bl_rna" in panel.__dict__:
                    bpy.utils.unregister_class(panel)
                    info_panels.pop(panel.__name__)

                # Register panel and keep reference
                bpy.utils.register_class(panel)
                info_panels[panel.__name__] = panel

        except Exception as e:
            bpy.ops.wm.report_message(type="WARNING", message=str(e))

    else:  # Delete previously created panels
        for panel in info_panels.values():
            bpy.utils.unregister_class(panel)

        info_panels.clear()


# ======== Handlers ===========


@persistent
def frame_change_update(scene):
    """Run everytime a frame is changed but the animation is not playing.

    Keep dynamic information up-to-date
    """
    if not bpy.data.screens["Main"].is_animation_playing:
        # Deselect the previous sequence if is not updated to avoid selecting them all while playing
        previous_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if previous_sequence:
            previous_sequence.select = previous_sequence.get("updated", False)

        # Update the current sequence
        current_sequence = get_current_sequence()

        # Set the current sequence as active
        bpy.data.scenes["Scene"].sequence_editor.active_strip = current_sequence

        if current_sequence:
            media_sequence = get_media_sequence(current_sequence)

            # Select the current sequence
            current_sequence.select = True

            # Change status selector value to sequence's
            # -------------------

            # Keep the review session state because changing the media status is supposed to start it
            keep_review_session_state = scene.review_session_active

            # Set the "change media status" tool to the current sequence status
            sequence_status = media_sequence.get("status")
            if sequence_status:
                scene.media_status = sequence_status

            # Set back the review session state
            scene.review_session_active = keep_review_session_state
            # --------------------

            # Show/Hide annotations depending on the lock
            bpy.data.screens["Main"].areas[1].spaces[0].show_annotation = (
                scene.all_annotations_displayed and not media_sequence.lock
            )

            # Check if is reviewed
            set_reviewed_state(media_sequence)

            # Update media info panels
            update_media_info_panels(media_sequence)
