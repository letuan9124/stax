# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""All primarily executed functions to register Stax into Blender

Uses :class:`AppStateStore` to wipe unnecessary/overriden native Blender UI.
"""
import sys
from pathlib import Path
import inspect
from importlib import import_module


import bpy
import bpy.utils.previews
import bl_app_override
import bl_ui
from bl_ui.space_toolsystem_common import ToolDef
from bl_app_override.helpers import AppOverrideState
from bpy.app.handlers import persistent


# Add folder for Stax dependencies in Stax template directory
python_dependencies = Path(__file__).parent.parent.joinpath("stax_dependencies")

# Add path to python path
sys.path.append(str(python_dependencies))


from .utils.utils_core import install_dependency

# Install Stax dependencies
stax_dependencies = Path(__file__).parent.joinpath("requirements.txt")
with open(stax_dependencies, "r") as f:
    stax_dependencies = f.read().split()

for dependency in stax_dependencies:
    # Extract module name
    module_name = dependency.split("==")[0]

    try:
        import_module(module_name)

    except ModuleNotFoundError:
        install_dependency(dependency, python_dependencies)

        # Import installed module
        import_module(module_name)


# Make Callbacks class accessible
from .Callbacks_Script import Callbacks

from .utils import utils_config, utils_linker
from .ops import (
    ops_cache,
    ops_export,
    ops_general,
    ops_timeline,
    ops_sequencer,
    ops_session,
    ops_version,
    ops_viewer,
)
from .draw import draw_sequencer
from .ui import (
    ui_configuration,
    ui_drawing,
    ui_export,
    ui_topbar,
    ui_sequencer,
    ui_texteditor,
    ui_time,
)
from .properties import (
    properties_core,
    properties_export,
    properties_sequencer,
)
from .utils import utils_ui


version = "2.4.4"
bl_minor_version = (2, 83)

# Custom images/icons
preview_collections = {}


class AppStateStore(AppOverrideState):
    """Wipe Blender UI

    Can keep some parts if needed
    """

    @staticmethod
    def class_ignore():
        """Remove all returned classes"""
        classes = []

        # Remove Sequencer Panels
        classes.extend(
            bl_app_override.class_filter(
                bpy.types.Panel,
                # Match any of these values
                bl_space_type={"SEQUENCE_EDITOR", "PROPERTIES", "VIEW_3D"},
                # Keep some panels
                blacklist={
                    # Sequencer
                    "SEQUENCER_PT_adjust",  # Strip params
                    "SEQUENCER_PT_adjust_comp",  # Strip params comp
                    "SEQUENCER_PT_strip",  # Strip info
                    "SEQUENCER_PT_effect",  # Effect params
                    "SEQUENCER_PT_tools_active",  # Left panel tools
                    "SEQUENCER_PT_source",  # Timeline strip source
                    "SEQUENCER_PT_custom_props",  # Timeline strip custom properties
                    "TOPBAR_PT_annotation_layers",  # Annotation layers
                    # Rendering
                    "RENDER_PT_output",
                    "RENDER_PT_encoding",
                    "RENDER_PT_encoding_video",
                    "RENDER_PT_encoding_audio",
                    "RENDER_PT_ffmpeg_presets",
                    # 3D view for advanced GP
                    "VIEW3D_PT_tools_active",  # 3D view Left panel tools
                },
            ),
        )

        # Remove Headers
        classes.extend(
            bl_app_override.class_filter(
                bpy.types.Header,
                # Match any of these values
                bl_space_type={
                    "SEQUENCE_EDITOR",
                    "TOPBAR",
                    "TEXT_EDITOR",
                    "VIEW_3D",
                    "DOPESHEET",
                    "DOPESHEET_EDITOR",
                },
                # blacklist={"VIEW3D_HT_tool_header",}, TODO Waiting AppOverrideState patch
            ),
        )

        # Remove Menus
        classes.extend(
            [
                bl_ui.space_view3d.VIEW3D_MT_gpencil_copy_layer,
                bl_ui.space_view3d.VIEW3D_MT_transform_base,
                bl_ui.space_dopesheet.DOPESHEET_MT_editor_menus,
            ]
        )

        classes.extend(
            bl_app_override.class_filter(
                bpy.types.Menu,
                # Match any of these values
                bl_label={"File", "New File"},
            ),
        )

        # #####
        # Customize 3D View GPencil Painting Tools
        # TODO implement this in the AppOverrideState instead of Stax only, submit patch
        # #####
        toolbar = bl_ui.space_toolsystem_toolbar
        tools_active = bl_ui.space_toolsystem_toolbar.VIEW3D_PT_tools_active

        # Tools to remove
        # ===============

        def override_tools(
            tool_mode: str, tools_to_replace: dict, tools_to_remove: list
        ):
            """Override native Blender tools available in tool modes context.

            Tools can be replaced or removed.

            :param tool_mode: Tool Mode to override
            :param tools_to_replace: Tools to replace. {tool_to_replace: replacing_tool}
            :param tools_to_remove: Tools to remove
            :return: Custom toolset
            """
            custom_tools = []
            for source_tool in tools_active._tools[tool_mode]:
                # Replace tool
                source_tool_replaced = False
                for tool_to_replace, replacing_tool in tools_to_replace.items():
                    if source_tool is tool_to_replace:
                        custom_tools.append(replacing_tool)
                        source_tool_replaced = True

                # Add source tool if not removed
                if not source_tool_replaced and source_tool not in tools_to_remove:
                    custom_tools.append(source_tool)

            return custom_tools

        # Mode: PAINT_GPENCIL
        # -------------------

        # List to remove tools from the original list
        remove = [
            toolbar._defs_view3d_generic.cursor,
            toolbar._defs_gpencil_paint.eyedropper,
        ]

        remove.extend(tools_active._tools_annotate)

        # Replace tool generation function
        # --------------------------------
        # Override source functions with some hacks to avoid the unwanted tools
        # This is not a clean way but it's seems to be the only one for now
        def generate_from_enum_ex(
            _context,
            *,
            idname_prefix,
            icon_prefix,
            type,
            attr,
            cursor="DEFAULT",
            tooldef_keywords={},
        ):
            tool_defs = []
            for enum in type.bl_rna.properties[attr].enum_items_static:
                name = enum.name
                idname = enum.identifier

                if idname != "FILL":  # Hack to disable the FILL tool
                    tool_defs.append(
                        ToolDef.from_dict(
                            dict(
                                idname=idname_prefix + name,
                                label=name,
                                icon=icon_prefix + idname.lower(),
                                cursor=cursor,
                                data_block=idname,
                                **tooldef_keywords,
                            )
                        )
                    )
            return tuple(tool_defs)

        def generate_from_brushes(context):
            return generate_from_enum_ex(
                context,
                idname_prefix="builtin_brush.",
                icon_prefix="brush.gpencil_draw.",
                type=bpy.types.Brush,
                attr="gpencil_tool",
                cursor="DOT",
                tooldef_keywords=dict(
                    operator="gpencil.draw",
                ),
            )

        replace = {
            bl_ui.space_toolsystem_toolbar._defs_gpencil_paint.generate_from_brushes: generate_from_brushes
        }

        # Update the source list
        tools_active._tools["PAINT_GPENCIL"] = override_tools(
            "PAINT_GPENCIL", replace, remove
        )

        # Mode: EDIT_GPENCIL
        # -------------------

        # List to remove tools from the original list
        remove = [
            toolbar._defs_view3d_generic.cursor,
            toolbar._defs_gpencil_edit.transform_fill,
        ]

        remove.extend(tools_active._tools_annotate)

        # Update the source list
        tools_active._tools["EDIT_GPENCIL"] = override_tools("EDIT_GPENCIL", {}, remove)

        return classes

    # ----------------
    # UI Filter/Ignore

    @staticmethod
    def ui_ignore_classes():
        """Classes to look into for UI disabling"""
        return (
            bpy.types.Header,
            bpy.types.Menu,
            bpy.types.Panel,
        )

    @staticmethod
    def ui_ignore_property(ty, prop):
        """Disable UI properties"""
        return (ty, prop) in {
            # Clean drawing toolbar
            ("BrushGpencilSettings", "use_material_pin"),  # Disable pinning
            (
                "BrushGpencilSettings",
                "vertex_mode",
            ),  # Disable vertex color choice
            (
                "BrushGpencilSettings",
                "vertex_color_factor",
            ),
            ("GpPaint", "color_mode"),  # Disable color mode (material or vertex)
            ("GPencilLayer", "use_lights"),
            ("GPencilLayer", "use_mask_layer"),
        }

    @staticmethod
    def ui_ignore_operator(op_id):
        """Disable UI operators"""
        return op_id in {
            "gpencil.layer_isolate",
        }

    # TODO: to refactor when the upcoming AppOverrideState will land
    # @staticmethod
    # def ui_ignore_template(ty):
    #     """Disable template"""
    #     return ty in {"template_header"}


app_state = AppStateStore()


@persistent
def load_handler(*kwargs):
    """Handler function to be run after every .blend loading

    Must contain every setup functions.
    """
    scene = bpy.context.scene
    user_prefs = scene.user_preferences

    # Load keymap
    bpy.ops.preferences.keyconfig_import(
        filepath=str(Path(__file__).parent.joinpath("stax_keymap.py"))
    )

    # Load user configuration
    utils_config.load_user_config()

    # Auto-login
    if user_prefs.user_auto_login:
        bpy.ops.wm.pm_authentication(
            login=user_prefs.user_login,
            password=user_prefs.user_password,
            save_credentials=user_prefs.user_save_credentials,
            auto_login=user_prefs.user_auto_login,
        )

    # Startup settings
    # ----------------

    # Default color space
    scene.view_settings.view_transform = "Standard"

    # World color to white for Onion skinning display
    scene.world.color = (1.0, 1.0, 1.0)

    # Drawings to front to avoid drawing artifacts
    scene.tool_settings.gpencil_sculpt.lock_axis = "AXIS_Y"
    # ----------------

    # Watch any action considered to start the review session
    bpy.ops.wm.watch_review_start()


def register():
    # Check if the Blender version is correct
    if bpy.app.version[:-1] != bl_minor_version:
        sys.tracebacklimit = 0
        raise ValueError(
            f"Blender version is not appropriate. Please use version {bl_minor_version[0]}.{bl_minor_version[1]}.*"
        )

    print("Template Register", __file__)

    app_state.setup()

    utils_ui.override_annotation_layers()
    utils_ui.setup_custom_icons()

    properties_core.register()
    properties_export.register()
    properties_sequencer.register()

    ops_cache.register()
    ops_export.register()
    ops_general.register()
    ops_sequencer.register()
    ops_session.register()
    ops_timeline.register()
    ops_version.register()
    ops_viewer.register()

    ui_configuration.register()
    ui_export.register()
    ui_drawing.register()
    ui_topbar.register()
    ui_sequencer.register()
    ui_texteditor.register()
    ui_time.register()

    draw_sequencer.register()

    # Run register() from callbacks script to override Stax's classes
    for c in utils_linker.get_callbacks_scripts():
        # Test if callback is the right one
        linker = utils_linker.get_linker()
        if linker and c.stem == linker.__module__:
            mod = utils_linker.load_script_as_module(c)
            mod.register()

    bpy.app.handlers.load_post.append(load_handler)

    # Custom Stax icons
    # ------------------

    # Create a new previews collection for the custom icons
    icons_previews = bpy.utils.previews.new()

    # Path to the folder where icons are
    icons_dir = Path(__file__).parent.parent.parent.joinpath("icons").resolve()

    for child in icons_dir.iterdir():
        if child.suffix == ".png":
            # Load a preview thumbnail of a file and store in the previews collection
            icons_previews.load(
                child.stem,
                str(child),
                "IMAGE",
            )

    preview_collections["icons"] = icons_previews


def unregister():
    print("Template Unregister", __file__)

    bpy.app.handlers.load_post.remove(load_handler)

    # Run unregister() from callbacks script to set back Stax's classes
    callbacks_module = sys.modules.get(
        inspect.getmodule(utils_linker.get_linker()).__name__
    )
    callbacks_module.unregister()

    properties_core.unregister()
    properties_export.unregister()
    properties_sequencer.unregister()

    ops_cache.unregister()
    ops_export.unregister()
    ops_general.unregister()
    ops_sequencer.unregister()
    ops_session.unregister()
    ops_timeline.unregister()
    ops_version.unregister()
    ops_viewer.unregister()

    ui_export.unregister()
    ui_drawing.unregister()
    ui_configuration.unregister()
    ui_topbar.unregister()
    ui_sequencer.unregister()
    ui_texteditor.unregister()
    ui_time.unregister()

    draw_sequencer.unregister()

    app_state.teardown()
